@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Store Approval</h1>
@stop

@section('content')

<div class="well" action="post">

    <fieldset>

	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
            		<td>Issuer Name</td>
	    			<td>Requisition Id</td>
            		<td>Action</td>
            		<td>Status</td>
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $finrequisitions as $finrequisition )
	    	<tbody>
	    		<tr>
		            <td><?php echo ++$i; ?></td>
		            <td>{{ $finrequisition->user->name }}</td>
	    			<td>{{ $finrequisition->id }}</td>
            		<td>
		              <a href="{{route('finapproval.show',$finrequisition->id) }}" class="btn btn-info">View</a>
		               <?php if(Auth::id()!= $finrequisition->user->id && $finrequisition->meta->formattedStatus =='Pending') { ?>
		               <form id="form-1" action="{{ route( 'finapproval.approve', $finrequisition->id ) }}" method="post" style="display:inline-block;">
				          @csrf
				          @method('put')

		              <button type="submit" href="{{ route( 'finrequisition.approve', $finrequisition->id ) }}" class="btn btn-success">Approve</button>
		              </form>

		              <form id="form-2" action="{{ route( 'finrequisition.reject', $finrequisition->id ) }}" method="post" style="display:inline-block;">
		              	@csrf
				        @method('put')
				      <button type="submit" href="" class="btn btn-danger">Reject</button>
				      </form>
				     <?php } ?>
				  </td>
				  {{-- <td>{{$finrequisition->formatted_status}}</td> --}}
				  <td>
				  	<h4>
				  		<span class="label label-<?php if($finrequisition->meta->formattedStatus=='Approved') {print 'success';}  elseif($finrequisition->meta->formattedStatus=='Rejected') {print 'danger';} elseif($finrequisition->meta->formattedStatus=='Material Received') {print 'primary';}elseif($finrequisition->meta->formattedStatus=='Material Sent') {print 'default';}else{print 'info';}?>">{{$finrequisition->meta->formattedStatus}}</span>
				  	</h4>
				  </td>
	    		</tr>
	    	</tbody>
	    	@endforeach
	    </table>

  	</fieldset>
</div>
@stop

@section('js')
<script>

</script>
@stop