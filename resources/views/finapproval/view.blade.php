@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    {{-- <h1>Test</h1> --}}
@stop

@section('content')
{{-- <form action="{{ route( 'finapproval.approve', $finrequisition->id ) }}" method="post" id="form-1">
  @csrf
  @method('put')
</form>
<form action="{{ route( 'finapproval.reject', $finrequisition->id ) }}" method="post" id="form-2">
  @csrf
  @method('put')
</form> --}}
<form action="{{ route( 'finapproval.send', $finrequisition->id ) }}" method="post" id="form-3">
  @csrf
  @method('post')
</form>
<div class="well">
  <fieldset>

    <h3>Store Approval</h3>

    <div class="form-group row">
      <label class="col-md-2 control-label">Issuer Name</label>
        <div class="col-md-4">
             {{ $finrequisition->user->name }}
        </div>
    </div>

    <div class="form-group row">
      <label class="col-md-2 control-label">Supplier Name</label>
        <div class="col-md-4">
             {{ $finrequisition->user->name }}
        </div>
    </div>

    <table class="table table-sm">
      <thead>
        <tr>
          <td>#</td>
          <td>Item Name</td>
          <td style=" margin-right:200px;">Quantity</td>
          <td style=" margin-right:200px;">Approve Quantity</td>
          <td style=" margin-right:200px;">Total Required</td>
        </tr>
      </thead>
      <tbody class="items">
     @php $serial = 0; @endphp
       @foreach( $finrequisition->items as $fgRequisitionItem )
        @if( $fgRequisitionItem->item && $fgRequisitionItem->item->formula )
          @foreach( $fgRequisitionItem->item->formula->formula_item as $formulaItem )
            {{-- @if( $formulaItem->raw_item ) --}}
              @php $raw_item = $formulaItem->raw_item; @endphp
              <tr class="item">
              <td>{{ ++$serial }}</td>
              <td>
                {{ $raw_item->item_name }}
              </td>
              <td>
                Requisition Item: {{ $qty_raw=$fgRequisitionItem->qty }}
                <br>
                Formula Item: {{ $qty_form=$formulaItem->amount }}
              </td>
              <td>{{ $fgRequisitionItem->aprv_qty }}</td>
              <td>
                <input type="text" name="itemrequired[{{ $raw_item->id }}]" form="form-3" value="{{ $total=$qty_raw*$qty_form }}" readonly="">
                {{-- <input type="hidden" name="itemid[]" form="form-3" value="{{ $raw_item->id }}" readonly=""> --}}
              </td>
             </tr>

            {{-- @endif --}}
          @endforeach
        @endif
      @endforeach
      </tbody>
    </table>

    <div class="form-group">
        <center>
       @if(Auth::id()!= $finrequisition->user->id && $finrequisition->meta->formattedStatus =='Approved')
          <button type="submit" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-danger" form="form-3">Sent</button>
        @endif
             <a href="{{ route('finapproval.index') }}" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-primary">Back</a>
        </center>
    </div>

  </fieldset>
</div>
@endsection
