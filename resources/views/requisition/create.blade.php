@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Test</h1>
@stop

@section('content')

<form class="well" action="{{ route('requisition.store') }}" method="post">
	{{ csrf_field() }}

    <fieldset>

	    <h3>Requisition</h3>

	    <div class="form-group row">
	    	<label class="col-md-2 control-label">Issuer Name</label>
	        <div class="col-md-4">
               <input id="name" name="name" class="form-control" required="true" type="text" value="{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}" readonly/> 
	        </div>
     	</div>

        <div class="form-group row">
        	<div class="col-md-2">
  				  <button type="button" class="btn btn-primary item-add">+</button>
        	</div>
     	  </div>

	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Item Name</td>
	    			<td style=" margin-right:200px;">Quantity</td>
{{-- 	    			<td>Price</td>
	    			<td>Subtotal</td> --}}
	    		</tr>
	    	</thead>
	    	<tbody class="items"></tbody>
{{-- 	    	<tfoot>
	    		<tr>
           
	    			<td colspan="3">&nbsp;</td>
	    			<td align="right">Total</td>
	    			<td align="right">325</td>
	    		</tr>
	    	</tfoot> --}}
	    </table>

     	<div class="form-group">
      		<center>
      			<button name="create" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Submit</button>
        		{{-- <a href="" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-primary">Back</a> --}}
      		</center>
     	</div>

  	</fieldset>
</div>
@stop

{{-- @section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop --}}

@section('js')
<script>
	$( document ).ready( function() {

		$( document ).on( 'click', '.item-remove', function() {
      $( this ).parent().parent().remove();
    } );

		$( '.item-add' ).click( function() {
			$( '.items' ).append( '<tr class="item">\
    			<td>\
    				<button type="button" class="btn btn-sm btn-danger item-remove">-</button>\
    			</td>\
          <td>\
            <select style="width:250px;" class="input-group form-control" name="item[][name]" required>\
                  <option value="">Select One</option>\
                  @foreach($items as $item)\
                  <option value="{{$item->id}}">{{$item->item_name}}</option>\
                  @endforeach\
              </select>\
          </td>\
    			<td>\
    				<input type="text" id="quantity" name="item[][quantity]" placeholder="Quantity" class="form-control" required/>\
    			</td>\
    		</tr>' );
		} );

    // $( document ).on( 'keyup', '#quantity', function() {

    //   var quantity_e = $( this );
    //   var price_e = quantity_e.parent().parent().children( 'td' ).eq( 3 ).find( '#price' );
    //   var subtotal_e = quantity_e.parent().parent().children( 'td' ).eq( 4 ).find( '#subtotal_s' );

    //   var quantity_v = parseInt( quantity_e.val() );
    //   var price_v = parseInt( price_e.val() );
    //   var subtotal_v = quantity_v * price_v;

    //   subtotal_e.text( subtotal_v );

    // } );

    // $( document ).on( 'keyup', '#price', function() {

    //   var price_e = $( this );
    //   var quantity_e = price_e.parent().parent().children( 'td' ).eq( 2 ).find( '#quantity' );
    //   var subtotal_e = price_e.parent().parent().children( 'td' ).eq( 4 ).find( '#subtotal_s' );

    //   var quantity_v = parseInt( quantity_e.val() );
    //   console.log( 'quantity ' + quantity_v );
    //   var price_v = parseInt( price_e.val() );
    //   console.log( 'price ' + price_v );
    //   var subtotal_v = quantity_v * price_v;
    //   console.log( 'subtotal ' + subtotal_v );

    //   subtotal_e.text( subtotal_v );

    // } );

	} );


</script>
@stop