@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Test</h1>
@stop

@section('content')

<form class="well" action="" method="post">
	{{ csrf_field() }}

    <fieldset>

	    <h3>Requisition</h3>

	    <div class="form-group row">
	    	<label class="col-md-2 control-label">Issuer Name</label>
	        <div class="col-md-4">
              	<input id="issuer_name" name="issuer_name" placeholder="issuer_name" class="form-control" required="true" type="text" readonly/>
	        </div>
     	</div>

        <div class="form-group row">
        	<div class="col-md-4">
  				<button type="button" class="btn btn-primary item-add">+</button>
        	</div>
     	</div>
       <tr class="item">
          <td>
            <button type="button" class="btn btn-sm btn-danger item-remove">-</button>\
          </td>
          <td>
            <input type="text" name="item[][name]" placeholder="Item Name" class="form-control" required/>
          </td>
          <td>
            <input type="text" name="item[][quantity]" placeholder="Quantity" class="form-control" required/>
          </td>
          <td>
            <input type="text" name="item[][price]" placeholder="Price" class="form-control" required/>
          </td>
          <td align="right">
            <span id="">&nbsp;</span>
            <input id="" type="hidden" name="item[][subtotal]" required/>
          </td>
        </tr>


	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Item Name</td>
	    			<td>Quantity</td>
	    			<td>Price</td>
	    			<td>Subtotal</td>
	    		</tr>
	    	</thead>
	    	<tbody class="items"></tbody>
	    	<tfoot>
	    		<tr>
	    			<td colspan="3">&nbsp;</td>
	    			<td align="right">Total</td>
	    			<td align="right">325</td>
	    		</tr>
	    	</tfoot>
	    </table>

  	</fieldset>
</form>
