@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Requisition</h1>
@stop

@section('content')

<div class="well">
    		<a href="{{ route('requisition.create') }}" class="btn btn-success">Create</a>

    <fieldset>

	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
            		<td>Issuer Name</td>
	    			<td>Total Item</td>
	    			<td>Total Price</td>
            		<td>Action</td>
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $items as $item )
	    	<tbody>
	    		<tr>
		            <td><?php echo ++$i; ?></td>
		            <td>{{ $item->user_id }}</td>
	    			<td></td>
	    			<td></td>
            		<td>
		              <a href="" class="btn btn-info">View</a>
		              <a href="" class="btn btn-warning">Edit</a>
		              <form action="" method="post" style="display:inline-block;">
		              @csrf
		              @method('delete')
		              <button type="submit" class="btn btn-danger" onclick="if(confirm('Are you sure?')==0){return false;}">Delete</button>
		              </form>
		              <a href="" class="btn btn-success">Approve</a>
				      <a href="" class="btn btn-danger">Reject</a>
				  </td>
	    		</tr>
	    	</tbody>
	    	@endforeach
	    </table>

  	</fieldset>
</div>
@stop

{{-- @section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop --}}

@section('js')
<script>

</script>
@stop