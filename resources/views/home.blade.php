@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-list"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">TOTAL REQUISITIONS</span>
              <span class="info-box-number">{{$req_count}}<small></small></span>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-box"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">INGREDIENT STOCK</span>
              <span class="info-box-number">{{$item_count}}</span>
            </div>
          </div>
        </div>

        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">TOTAL ORDER</span>
              <span class="info-box-number">{{$order_count}}</span>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">USERS</span>
              <span class="info-box-number">{{$user_count}}</span>
            </div>
          </div>
        </div>
    </div>
</div>

<div class="well" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
<div class="container">
  <h3 align="center" style="font-family:; ">Order Details</h3>
    <div class="row">
      <center>
        @foreach( $floors as $floor )
        <div class="col-md-12">
          <span class="badge"><h5>{{ $floor->floor_name}}</h5></span>
            <div class="row">
              @foreach( $floor->rooms as $room )
              <div class="col-md-6">
                <div>
                  <h3><span class="label label-info">{{ $room->room_name }}</span></h3>
                  <div>
                    <div class="row">
                      @foreach( $room->tables as $table )

                      <div class="col-md-2">
                        <?php if($table->status==0) { ?>
                         <h3 style="position: relative;"><span style="font-size: 20px; "  class="badge btn btn-success"><img width="50px;" src="img/table.png"></span><span style="position: absolute;top: 50%;left: 50%; transform: translate(-50% , -50%);color: #ffffff;">{{ $table->table_name}}</span></h3>
                        <?php } else {?>
                          <h3 style="position: relative;"><span style="font-size: 20px; "  class="badge btn btn-danger"><img width="50px;" src="img/reserve.png"></span><span style="position: absolute;top: 50%;left: 50%; transform: translate(-50% , -50%);color: #ffffff;">{{ $table->table_name}}</span></h3>
                      <?php } ?>
                     </div>
                      <div class="col-md-1"></div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
        </div>
        @endforeach
      </center>
    </div>
</div>
</div>
@endsection
