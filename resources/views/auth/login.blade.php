@extends('layouts.app')

@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}



<div class="container">
   <section id="formHolder">

      <div class="row">

         <!-- Brand Box -->
         <div class="col-sm-6 brand">
            <a href="#" class="logo">
                <img src="{{asset('/img/res.png')}}" alt="logo" width="80" height="80">
            </a>

            <div class="heading">
               <h2 style="font-size: 35px;">Restaurant Management System</h2>
               <p></p>
            </div>

            <div class="success-msg">
               <p>Great! You are one of our members now</p>
               <a href="#" class="profile">Your Profile</a>
            </div>
         </div>


         <!-- Form Box -->
         <div class="col-sm-6 form">

            <!-- Login Form -->
            <div class="login form-peice switched">
               <form class="login-form" method="POST" action="{{ route('login') }}">
                @csrf
                  <div class="form-group">
                     {{-- <label for="loginemail">Email Adderss</label> --}}
                     <label for="email">{{ __('E-Mail Address') }}</label>
                     {{-- <input type="email" name="loginemail" id="loginemail" required> --}}
                  <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus/>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                  </div>

                  <div class="form-group">
                     {{-- <label for="loginPassword">Password</label> --}}
                     <label for="password">{{ __('Password') }}</label>
                     {{-- <input type="password" name="loginPassword" id="loginPassword" required> --}}
                     <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required/>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('password') }}</strong></span>
                    @endif
                  </div>
                   {{-- <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div> --}}
                  <div class="CTA">
                     {{-- <input type="submit" value="Login"> --}}
                      <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>

                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}"></a>
                        @endif
                     <a href="#" class="switch">I'm New</a>
                  </div>
               </form>
            </div><!-- End Login Form -->


            <!-- Signup Form -->
            <div class="signup form-peice">
            {{-- <div class="card-header">{{ __('Register') }}</div> --}}
               {{-- <form class="signup-form" action="#" method="post"> --}}
               <form class="signup-form" method="POST" action="{{ route('register') }}">
               @csrf
                  <div class="form-group">
                     {{-- <label for="name">Full Name</label> --}}
                     <label for="name">{{ __('Name') }}</label>
                     {{-- <input type="text" name="username" id="name" class="name"> --}}
                     <input id="name" type="text" class="name{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                     <span class="error"></span>
                  </div>

                  <div class="form-group">
                     {{-- <label for="email">Email Adderss</label> --}}
                     <label for="email">{{ __('E-Mail Address') }}</label>
                     {{-- <input type="email" name="emailAdress" id="email" class="email"> --}}
                     <input id="email" type="email" class="email{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                     <span class="error"></span>
                  </div>

                 {{--  <div class="form-group">
                     <label for="phone">Phone Number - <small>Optional</small></label>
                     <input type="text" name="phone" id="phone">
                  </div> --}}

                  <div class="form-group">
                     {{-- <label for="password">Password</label> --}}
                     <label for="password">{{ __('Password') }}</label>
                     {{-- <input type="password" name="password" id="password" class="pass"> --}}
                     <input id="password" type="password" class="pass{{ $errors->has('password') ? ' is-invalid' : '' }}" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                     <span class="error"></span>
                  </div>

                  <div class="form-group">
                     {{-- <label for="passwordCon">Confirm Password</label> --}}
                     <label for="password-confirm">{{ __('Confirm Password') }}</label>

                     {{-- <input type="password" name="passwordCon" id="passwordCon" class="passConfirm"> --}}
                     <input id="password-confirm" type="password" class="passConfirm" name="password_confirmation" required>
                     <span class="error"></span>
                  </div>

                  <div class="CTA">
                     {{-- <input type="submit" value="Signup Now" id="submit"> --}}
                     <button type="submit" id="submit" class="btn btn-primary">{{ __('Register') }}</button>
                     <a href="#" class="switch">I have an account</a>
                  </div>
               </form>
            </div><!-- End Signup Form -->
         </div>
      </div>

   </section>

</div>

@endsection
@section('js')




@stop