@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    {{-- <h1>Test</h1> --}}
@stop

@section('content')

<form action="{{ route( 'approval.purchase', $rawrequisition->id ) }}" method="post" id="form-3">
  @csrf
  @method('put')
</form>
<div class="well">
  <fieldset>

    <h3>Purchase</h3>

    <div class="form-group row">
      <label class="col-md-2 control-label">Issuer Name</label>
        <div class="col-md-4">{{ $rawrequisition->user->name }}</div>
    </div>

    <table class="table table-sm">
      <thead>
        <tr>
          <td>#</td>
          <td>Item Name</td>
          <td style=" margin-right:200px;">
            Quantity
          </td>
          @if(Auth::id() != $rawrequisition->user->id)
            <td style=" margin-right:200px;">Approve Quantity</td>

            @if($rawrequisition->meta->formattedStatus =='Approved' || $rawrequisition->meta->formattedStatus =='Purchase Ordered')
              <td style=" margin-right:200px;">Price</td>
            @endif
            <td>Subtotal</td>
          @endif

        </tr>
      </thead>
      <tbody class="items">
        @foreach( $rawrequisition->items as $i => $item )
          <tr class="item">
            <td>{{ $i+1 }}</td>
            <td>
              {{ $item->item->item_name}}
            </td>
            <td>
              {{ $item->qty }}
            </td>
            @if(Auth::id()!= $rawrequisition->user->id )
              @if ( $rawrequisition->meta->formattedStatus =='Pending')

                <td>
                   <input type="text" name="rawrequisition_items[{{ $item->id }}][aprv_qty]" id="aprv_qty" placeholder="Approve Quantity" class="form-control" value="{{ $item->aprv_qty }}" style="width: 150px;" form="form-1" />
                </td>

              @else
                <td id="aprv_qty">
                  {{ $item->aprv_qty }}
                  <input type="hidden" name="rawrequisition_items[{{ $item->id }}][aprv_qty]" id="aprv_qty" value="{{ $item->aprv_qty }}" form="form-1" />
                </td>
              @endif
              @if ( $rawrequisition->meta->formattedStatus =='Approved')

                <td>
                   <input type="text" name="rawrequisition_items[{{ $item->id }}][item_price]" placeholder="Price" id="price" class="form-control" style="width: 150px;" form="form-3" required />
                </td>
              @else
                <td id="price">
                  {{ $item->item_price }}
                  <input type="hidden" name="rawrequisition_items[{{ $item->id }}][item_price]" id="price" value="{{ $item->item_price }}" form="form-3" />
                </td>
              @endif
              <td><input type="text" name="rawrequisition_items[{{ $item->id }}][subtotal]" id="subtotal" placeholder="Subtotal" class="form-control"  form="form-3" required readonly/></td>
            @endif
          </tr>
        @endforeach
      </tbody>
      <tfoot>
          <tr>
            <td colspan="4">&nbsp;</td>
            <td align="right">Total</td>
            <td align="right"><input type="text" name="total" id="total" placeholder="Total" class="form-control"  form="form-3" required readonly/></td>
          </tr>
        </tfoot>
    </table>
    <div class="form-group">
        <center>

          @if(Auth::id()!= $rawrequisition->user->id && $rawrequisition->meta->formattedStatus =='Approved')
          <button style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" href="{{ route( 'purchase.receive', $rawrequisition->id ) }}" class="btn btn-success" form="form-3">Purchase</button>
          @endif

          <a href="{{ route('purchase.index') }}" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-primary">Back</a>
        </center>
    </div>
  </fieldset>
</div>
@endsection

@section( 'js' )

<script>
  var total=0;
$( document ).on( 'keyup', '#price', function() {
  var price_e = $( this );
  var quantity_e = price_e.parent().prev().find( '#aprv_qty' );
  var subtotal_e = price_e.parent().next().find( '#subtotal' );
  var price_v = price_e.val();
  var quantity_v = quantity_e.val();
  var subtotal_ov= subtotal_e.val();
  var subtotal_nv = price_v * quantity_v;
  subtotal_e.val( subtotal_nv );

  total += Number(subtotal_nv)-Number(subtotal_ov);
  $('#total').val(total);
}
 );

</script>
@endsection