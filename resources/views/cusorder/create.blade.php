@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
   <h1>Item Create</h1>
@stop

@section('content')

<form class="well" action="{{ route( 'cusorder.store') }}" method="post" enctype="multipart/form-data">

 {{ csrf_field() }}

  <fieldset>

    <div class="form-group row">

      <label class="col-md-2">Table Name</label>
        <div class="col-md-2">
          <select class="input-group form-control " name="cusorder[table_id]" id="table_id" autocomplete="off" >
            <option value="">Select One</option>
            @foreach($tables as $table)
            <option value="{{$table->id}}">{{$table->table_name}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-2"></div>
        <div class="form-group row">
          <div class="col-md-2">
            <button type="button" class="btn btn-primary item-add">+</button>
          </div>
        </div>
    </div>

    <table class="table table-sm">
        <thead>
          <tr>
            <td>#</td>
            <td>Item Name</td>
            <td>Quantity</td>
            <td>Price</td>
            <td>Subtotal</td>
          </tr>
        </thead>
        <tbody class="items"></tbody>
        <tr>
          <td colspan="4"></td>
          <td>
            <input class="form-control total" style="width: 290px;" type="text" name="cusorder[total]" id="total" placeholder="Total" autocomplete="off" required readonly/>
          </td>
        </tr>
    </table>


      <div class="form-group">
          <center>
            <button style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Submit</button>
          </center>
      </div>

  </fieldset>

</form>

@stop
@section('js') 
<script>
  $( document ).ready( function() {

    var i = 0;

    $( document ).on( 'click', '.item-remove', function() {
      $( this ).parent().parent().remove();
    } );

    $( '.item-add' ).click( function() {
      $( '.items' ).append( '<tr class="item">\
          <td>\
            <button type="button" class="btn btn-sm btn-danger item-remove">-</button>\
          </td>\
          <td>\
            <select class=" input-group form-control cusorder_details'+i+'" style="width:250px;" name="cusorder_details['+i+'][item_id]" required>\
                  <option value="" selected>Select One</option>\
                  @foreach($items as $item)\
                  <option value="{{$item->id}}">{{$item->item_name}}</option>\
                  @endforeach\
              </select>\
          </td>\
          <td>\
            <input class="form-control rawrequisition_qty'+i+'" type="number" id="quantity" name="cusorder_details['+i+'][quantity]" width:10px; placeholder="Quantity" value="1"  required/>\
          </td>\
          <td>\
            <input type="text" id="price" name="cusorder_details['+i+'][price]" width:10px; class="form-control rawrequisition_price'+i+'" required readonly/>\
          </td>\
          <td>\
            <input type="text" id="subtotal" name="cusorder_details['+i+'][subtotal]" placeholder="" class="form-control rawrequisition_subtotal'+i+' sub_total" required readonly/>\
          </td>\
        </tr>\
        <script>\
          $(document).on(\'change\', \'.cusorder_details'+i+'\', function() {\
              var itemElem = $(this);\
              var item_id = itemElem.val();\
              $.ajax({\
                  url: "{{ route('cusordergetPrice') }}",\
                  method: "GET",\
                  data: {data:item_id},\
                  success: function(rdata){\
                    $(\'.rawrequisition_price'+i+'\').val(rdata);\
                    $(\'.rawrequisition_subtotal'+i+'\').val(rdata);\
                    addsubtotal();\
                  }\
              });\
          });\
          $( document ).on( \'keyup\', \'.rawrequisition_qty'+i+'\', function() {\
            var itemQty = $(this);\
            var qty = parseInt( itemQty.val() );\
            var itemPrice = $(\'.rawrequisition_price'+i+'\').val();\
            var subtotal = $(\'.rawrequisition_subtotal'+i+'\');\
            var newPrice = qty * itemPrice; \
            subtotal.val( newPrice );\
            addsubtotal();\
          } );\
          $( document ).on( \'wheel\', \'.rawrequisition_qty'+i+'\', function() {\
            var itemQty = $(this);\
            var qty = parseInt( itemQty.val() );\
            var itemPrice = $(\'.rawrequisition_price'+i+'\').val();\
            var subtotal = $(\'.rawrequisition_subtotal'+i+'\');\
            var newPrice = qty * itemPrice; \
            subtotal.val( newPrice );\
            addsubtotal();\
          } );\
          $( document ).on( \'change\', \'.rawrequisition_qty'+i+'\', function() {\
            var itemQty = $(this);\
            var qty = parseInt( itemQty.val() );\
            var itemPrice = $(\'.rawrequisition_price'+i+'\').val();\
            var subtotal = $(\'.rawrequisition_subtotal'+i+'\');\
            var newPrice = qty * itemPrice; \
            subtotal.val( newPrice );\
            addsubtotal();\
          } );\
          <\/script>\
        ' );
      i++;
    } );

  } );
  function addsubtotal(){
    var sum = 0;
    $('.sub_total').each(function(){
        sum += parseFloat(this.value);
    });
    $('.total').val(sum);
  };
</script>
@stop