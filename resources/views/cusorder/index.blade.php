@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
	<h1>Customer Order List
@stop

@section('content')

    <fieldset>

    	<div class="well">
    	<a href="{{ route('cusorder.create') }}" class="btn btn-success">Create</a>

	    <table class="table table-sm">

	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Table Name</td>
	    			<td>Total Price</td>
	    			<td>Discount</td>
	    			<td>Due</td>
	    			<td>Status</td>
	    			<td>Action</td>
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $cusorders as $cusorder )
	    	<tbody class="items">
		    		<td><?php echo ++$i; ?></td>
		    		<td>{{$cusorder->table->table_name}}</td>
		    		<td>{{$cusorder->total}}</td>
		    		<td>{{$cusorder->discount}}</td>
		    		<td>{{$cusorder->due}}</td>
		    		<td>{{$cusorder->status}}</td>
		    		<td>
			        	<a href="{{route('cusorder.edit',$cusorder->id) }}" class="btn btn-warning">Edit</a>
			        </td>

	    	</tbody>

	    	@endforeach

	    </table>

		</div>

  	</fieldset>

</form>

@stop