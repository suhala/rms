@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Requisition</h1>
@stop

@section('content')

<div class="well" action="post">

    		<a href="{{ route('rawrequisition.create') }}" class="btn btn-success">Create</a>

    <fieldset>

	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
            		<td>Issuer Name</td>
	    			<td>Requisition Id</td>
            		<td>Action</td>
            		<td>Status</td>
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $rawrequisitions as $rawrequisition )
	    	<tbody>
	    		<tr>
		            <td><?php echo ++$i; ?></td>
		            <td>{{ $rawrequisition->user->name }}</td>
	    			<td>{{ $rawrequisition->id }}</td>
            		<td>
		              <a href="{{route('rawrequisition.show',$rawrequisition->id) }}" class="btn btn-info">View</a>
		              <?php if(Auth::id()== $rawrequisition->user->id && $rawrequisition->meta->formattedStatus =='Pending')  { ?>
		              <a href="{{route('rawrequisition.edit',$rawrequisition->id) }}" class="btn btn-warning">Edit</a>
		               <?php } ?>
		               <?php if(Auth::id()!= $rawrequisition->user->id && $rawrequisition->meta->formattedStatus =='Pending') { ?>
		               <form id="form-1" action="{{ route( 'rawrequisition.approve', $rawrequisition->id ) }}" method="post" style="display:inline-block;">
				          @csrf
				          @method('put')

		              <button type="submit" href="{{ route( 'rawrequisition.approve', $rawrequisition->id ) }}" class="btn btn-success">Approve</button>
		              </form>

		              <form id="form-2" action="{{ route( 'rawrequisition.reject', $rawrequisition->id ) }}" method="post" style="display:inline-block;">
		              	@csrf
				        @method('put')
				      <button type="submit" href="" class="btn btn-danger">Reject</button>
				      </form>
				     <?php } ?>
				  </td>
				  {{-- <td>{{$rawrequisition->formatted_status}}</td> --}}
				  <td>
				  	<h4>
				  		<span class="label label-<?php if($rawrequisition->meta->formattedStatus=='Approved') {print 'success';}  elseif($rawrequisition->meta->formattedStatus=='Rejected') {print 'danger';} elseif($rawrequisition->meta->formattedStatus=='Received') {print 'primary';}elseif($rawrequisition->meta->formattedStatus=='Purchase Ordered') {print 'default';}else{print 'info';}?>">{{$rawrequisition->meta->formattedStatus}}</span>
				  	</h4>
				  </td>
	    		</tr>
	    	</tbody>
	    	@endforeach
	    </table>

  	</fieldset>
</div>
@stop

@section('js')
<script>

</script>
@stop