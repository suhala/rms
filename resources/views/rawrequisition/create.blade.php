@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Test</h1>
@stop

@section('content')

<form class="well" action="{{ route('rawrequisition.store') }}" method="post">
	{{ csrf_field() }}

    <fieldset>

	    <h3>Requisition</h3>

	    <div class="form-group row">
	    	<label class="col-md-2 control-label">Issuer Name</label>
	        <div class="col-md-4">
               <input id="name" name="rawrequisition[name]" class="form-control" required="true" type="text" value="{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}" readonly/>
	        </div>
     	</div>

        <div class="form-group row">
        	<div class="col-md-2">
  				  <button type="button" class="btn btn-primary item-add">+</button>
        	</div>
     	  </div>

	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Item Name</td>
	    			<td style=" margin-right:200px;">Quantity</td>
	    		</tr>
	    	</thead>
	    	<tbody class="items"></tbody>
	    </table>

     	<div class="form-group">
      		<center>
      			<button style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Submit</button>
      		</center>
     	</div>

  	</fieldset>
</div>
</form>
@stop

@section('js') 
<script>
	$( document ).ready( function() {

    var i = 0;

		$( document ).on( 'click', '.item-remove', function() {
      $( this ).parent().parent().remove();
    } );

		$( '.item-add' ).click( function() {
			$( '.items' ).append( '<tr class="item">\
    			<td>\
    				<button type="button" class="btn btn-sm btn-danger item-remove">-</button>\
    			</td>\
          <td>\
            <select style="width:250px;" class="input-group form-control" name="rawrequisition_items['+i+'][item_id]" required>\
                  <option value="" selected>Select One</option>\
                  @foreach($items as $item)\
                  <option value="{{$item->id}}">{{$item->item_name}}</option>\
                  @endforeach\
              </select>\
          </td>\
    			<td>\
    				<input type="text" id="quantity" name="rawrequisition_items['+i+'][qty]" placeholder="Quantity" class="form-control" required/>\
    			</td>\
    		</tr>' );
      i++;
		} );

	} );

</script>
@stop