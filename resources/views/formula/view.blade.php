@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
  <h1>Formula Items</h1>
@stop

@section('content')

    <fieldset>

	   {{--  <h3>Formula Items</h3> --}}

      <div class="form-group row">
        <label class="col-md-2">Item Name</label>
          <div class="col-md-4">
                <select style="width:250px;" class="input-group form-control" id="item_id" name="formulas[item_id]" required>
                  <option value="" selected>Select One</option>
                  {{-- @foreach($items as $item)
                  <option value="{{$item->id}}">{{$item->item_name}}</option>
                  @endforeach --}}
              </select>
          </div>
      </div>

      <div class="form-group row">

        <div class="form-group row">
        	<div class="col-md-4">
  				<button type="button" class="btn btn-primary ingredient-add">+</button>
        	</div>
     	</div>

	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Item Name</td>
	    			<td>Amount</td>
	    		</tr>
	    	</thead>
	    	<tbody class="ingredients"></tbody>
	    </table>

     	<div class="form-group">
      		<center>
      			<button name="create" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Submit</button>
        		{{-- <a href="" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-primary">Back</a> --}}
      		</center>
     	</div>

  	</fieldset>

@stop

{{-- @section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop --}}

@section('js')
<script>
	$( document ).ready( function() {
      var i = 0;

		$( document ).on( 'click', '.ingredient-remove', function() {
	        $( this ).parent().parent().remove();
	    } );

		$( '.ingredient-add' ).click( function() {
			$( '.ingredients' ).append( '<tr class="ingredient">\
    			<td>\
    				<button type="button" class="btn btn-sm btn-danger ingredient-remove">-</button>\
    			</td>\
    			<td>\
    				<select style="width:250px;" class="input-group form-control" name="formulas_item['+i+'][item_id]" required>\
                  <option value="" selected>Select One</option>\
   </select>\
    			</td>\
    			<td>\
    				<input type="text" name="formulas_item['+i+'][amount]" placeholder="Amount" class="form-control" required/>\
    			</td>\
    		</tr>' );
      i++;
		} );

	} );
</script>
@stop