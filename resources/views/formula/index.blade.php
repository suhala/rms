@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    {{-- <h1>Formula Items</h1> --}}
@stop

@section('content')
<div class="well">
	 <a href="{{ route('formula.create') }}" class="btn btn-success">Create</a>

    <fieldset>

	    <h3>Formula Items</h3>

	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Item name</td>
	    			<td>Total Ingredients</td>
	    			<td>Action</td>
	    		</tr>
	    	</thead>
	    	@foreach( $formulas as $formula )
	    	<tbody>
	    		<?php $i=0; ?>
	    		<td><?php echo ++$i; ?></td>
		        <td>{{ $formula->fin_item->item_name }}</td>
	    		<td></td>
	    		<td>
	    			<a href="{{ route('formula.show', $formula->id) }}" class="btn btn-info">View</a>
		              <a href="{{ route('formula.edit', $formula->id) }}" class="btn btn-warning">Edit</a>
		              <form action="{{ route('formula.destroy', $formula->id) }}" method="post" style="display:inline-block;">
				          @csrf
				          @method('delete')
				          <button type="submit" class="btn btn-danger" onclick="if(confirm('Are you sure?')==0){return false;}">Delete</button>
		              </form>
		        </td>
	    	</tbody>
	    	@endforeach
	    </table>

  	</fieldset>
</div>
@stop
