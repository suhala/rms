<!DOCTYPE HTML>
<html>
<head>
<meta  charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Restaurant Management System</title>

<!-- Favicons -->

<link rel="shortcut icon" href="{{ URL::asset('img/simec.png') }}">
<link rel="apple-touch-icon" href="{{ URL::asset('img/simec.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ URL::asset('img/simec.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ URL::asset('img/simec.png') }}">

<!-- Fonts -->

<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

<!-- Styles -->

<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet"  media="screen">

</head>
<body>

  <!-- Loader -->

  <div class="loader">
    <div class="loader-brand"><img style="width: 200px;" alt="" class="img-responsive center-block" src="img/brand.png"></div>
  </div>

  <div id="layout" class="layout">

    <!-- Header -->

    <header id="top" class="navbar js-navbar-affix animated slideInDown affix menu-gallary-header">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#layout" class="brand js-target-scroll">
            <img class="brand-img-white" alt="" style="width: 200px;" src="img/brand-white.png">
            <img class="brand-img" alt="" src="{{ URL::asset('img/brand.png') }}" width="100">
          </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse" aria-expanded="false">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="">Home</a></li>
            <li class=""><a href="#about">About</a></li>
            <li class=""><a href="#prices">Prices</a></li>
          </ul>
        </div>
      </div>
    </header>

    <!-- Home -->
    <div style="margin: 50px;"></div>
    <div class="content-sec">
      <div class="container">
        <div class="row">
          @foreach( $category->menu_items as $item )
          <div class="col-md-3"><center><img style="width:250px; height:250px; " class="rounded img-fluid" src="{{ asset($item->item_img) }}" alt=""><label>{{ $item->item_name }}</label></center></div>

          @endforeach
        </div>
      </div>
    </div>

    <!-- Content -->


    <!-- Footer -->

    <footer id="footer" class="footer text-center text-left-md bgc-light">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <div class="social">
              <a href="#" class="fa fa-facebook"></a>
              <a href="#" class="fa fa-twitter"></a>
              <a href="#" class="fa fa-pinterest"></a>
              <a href="#" class="fa fa-youtube-play"></a>
            </div>
          </div>
          <div class="col-md-7 text-right-md">
            <div class="copy">
              © 2019 Demo. All rights reserved by <a href="simecsystem.com" target="_blank">SIMEC System Ltd</a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>

  <!-- Modals -->

  <div id="request" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <span class="close" data-dismiss="modal" aria-label="Close">&times;</span>
            <h2 class="modal-title">Get start</h2>
          </div>
          <div class="modal-body text-center">
              <form class="form-request js-ajax-form">
                <div class="row-fields row">
                  <div class="form-group col-field">
                      <input type="text" class="form-control" name="name" required placeholder="Name *">
                  </div>
                   <div class="form-group col-field">
                      <input type="email" class="form-control" name="email" required placeholder="Email *">
                    </div>
                  <div class="form-group col-field">
                      <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                  </div>
                  <div class="col-sm-12">
                    <button type="submit" class="btn" data-text-hover="Submit">Send request</button>
                  </div>
                </div>
              </form>
          </div>
        </div>
    </div>
  </div>

  <!-- Modals success -->

  <div id="success" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></span>
            <h2 class="modal-title">Thank you</h2>
            <p class="modal-subtitle">Your message is successfully sent...</p>
          </div>
        </div>
    </div>
  </div>

  <!-- Modals error -->

  <div id="error" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></span>
             <h2 class="modal-title">Sorry</h2>
            <p class="modal-subtitle"> Something went wrong </p>
          </div>
        </div>
    </div>
  </div>

<!-- Scripts -->

<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/smoothscroll.js') }}"></script>
<script src="{{ URL::asset('js/interface.js') }}"></script>
</body>
</html>
