@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Test</h1>
@stop

@section('content')
<form action="{{ route( 'finrequisition.approve', $finrequisition->id ) }}" method="post" id="form-1">
  @csrf
  @method('put')
</form>
<form action="{{ route( 'finrequisition.reject', $finrequisition->id ) }}" method="post" id="form-2">
  @csrf
  @method('put')
</form>

<div class="well">
  <fieldset>

    <h3>Requisition</h3>

    <div class="form-group row">
      <label class="col-md-2 control-label">Issuer Name</label>
        <div class="col-md-4">
             {{ $finrequisition->user->name }}
        </div>
    </div>

    <div class="form-group row">
      <label class="col-md-2 control-label">Supplier Name</label>
        <div class="col-md-4">
             {{ $finrequisition->user->name }}
        </div>
    </div>

    <table class="table table-sm">
      <thead>
        <tr>
          <td>#</td>
          <td>Item Name</td>
          <td style=" margin-right:200px;">
            Quantity
          </td>
          @if(Auth::id() != $finrequisition->user->id)
          <td style=" margin-right:200px;">Approve Quantity</td>

          @if($finrequisition->meta->formattedStatus =='Approved' || $finrequisition->meta->formattedStatus =='Sale Ordered')
          <td style=" margin-right:200px;">Price</td>
          @endif
          @endif

        </tr>
      </thead>
     @foreach( $finrequisition->items as $i => $item )
      <tbody class="items">
        <tr class="item">
        <td>{{ $i+1 }}</td>
        <td>
          {{ $item->item->item_name}}
        </td>
        <td>
          {{ $item->qty }}
        </td>
        <?php if(Auth::id()!= $finrequisition->user->id )
        { if ( $finrequisition->meta->formattedStatus =='Pending')
        {  ?>
        <td>
           <input type="text" name="finrequisition_items[{{ $item->id }}][aprv_qty]" placeholder="Approve Quantity" class="form-control" value="{{ $item->qty }}" style="width: 150px;" form="form-1" />
        </td>
        <?php } else { ?>
        <td>{{ $item->aprv_qty }}</td>
        <?php }
        if ( $finrequisition->meta->formattedStatus =='Approved') 
        {  ?>
        <td>
           <input type="text" name="finrequisition_items[{{ $item->id }}][item_price]" placeholder="Price" class="form-control" style="width: 150px;" form="form-3" required />
        </td>
        <?php } else { ?>
        <td>{{ $item->item_price }}</td>
        <?php }
      } ?>
      </tr>
      </tbody>
      @endforeach
    </table>

    <div class="form-group">
        <center>

           <?php if(Auth::id()!= $finrequisition->user->id && $finrequisition->meta->formattedStatus =='Pending') { ?>
          <button type="submit" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success" form="form-1">Approve</button>

          <button style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" href="{{ route( 'finrequisition.reject', $finrequisition->id ) }}" class="btn btn-danger" form="form-2">Reject</button>
           <?php } ?>
             <a href="{{ route('finrequisition.index') }}" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-primary">Back</a>
        </center>
    </div>

  </fieldset>
</div>
@stop
