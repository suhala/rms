@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>CATEGORY CREATE</h1>
@stop

@section('content')
@include( 'partials.error' )
<form class="well" action="{{ route('category.store',$type) }}" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}

    <fieldset>

	    <div class="form-group row">
	    	<label class="col-md-2">Category</label>
	        <div class="col-md-3">
              	<input id="category_name" name="category_name" placeholder="Category Name" class="form-control" required type="text"/>
	        </div>

	        <label class="col-md-2">Parent Category</label>
	        <div class="col-md-3">
              	<select class="input-group form-control" name="sub_cat" id="sub_cat" >
		              <option value="">Select One</option>

		              @foreach($categories as $category)
		              <option value="{{$category->id}}">{{$category->category_name}}</option>
		              @endforeach
	            </select>
	        </div>
	        <div class="col-md-2">

	        <button style="display:inline-block;" name="create" class="btn btn-success">Add</button>

	        </div>
	    </div>

	    <div class="form-group row">
	    	<label class="col-md-2">Select Image</label>
	        <div class="col-md-3">
              	<input id="category_img" name="category_img" placeholder="" class="form-control" type="file"/>
	        </div>
	    </div>
     	</div>

  	</fieldset>
</form>
@stop
