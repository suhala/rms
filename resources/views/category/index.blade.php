@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>CATEGORY LIST</h1>
    <p>{{session('msg')}}</p>
@stop

@section('content')
    <fieldset>
     <div class="well">
     	<a href="{{ route('category.create',$type) }}" class="btn btn-success">Create</a>
	    <table class="table table-sm">
	    	<thead>
	    		<tr style="font-weight: bold;" >
	    			<td>#</td>
	    			<td>Category</td>
	    			<td>Parent Category</td>
	    			<td>Image</td>
	    			<td>Action</td>
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $categories as $category )
	    	<tbody>
	    			<td><?php echo ++$i; ?></td>
		    		<td>{{ $category->category_name }}</td>
		    		<td>{{ $category->parent ? $category->parent->category_name : '' }}</td>
		    		<td>
		    			<img src="{{ asset( $category->category_img ) }}" width="100">
		    		</td>
		    		<td>
					  {{-- <a href=" {{ route('category.show',[$type,$category->id]) }} " class="btn btn-info">View</a> --}}
		              <a href="{{ route('category.edit', [$type,$category->id]) }}" class="btn btn-warning">Edit</a>

		              <form action="{{ route('category.destroy', [$type,$category->id]) }}" method="post" style="display:inline-block;">
				          @csrf
				          @method('delete')
				          <button type="submit" class="btn btn-danger" onclick="if(confirm('Are you sure?')==0){return false;}">Delete
				          </button>
		              </form>
			        </td>
	    	</tbody>
			@endforeach
	    </table>
	</div>
  	</fieldset>

@stop
