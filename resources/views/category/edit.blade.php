@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>UPDATE CATEGORY</h1>
@stop

@section('content')
<form class="well" action="{{ route('category.update', [$type, $category->id]) }}" method="post">
	@csrf
    @method("put")

    <fieldset>

	    <div class="form-group row">
	    	<label class="col-md-2">Category</label>
	        <div class="col-md-3">
              	<input id="category_name" name="category_name" value="{{ $category->category_name }}" placeholder="Category Name" class="form-control" required type="text"/>
	        </div>

	        <label class="col-md-2">Parent Category</label>
	        <div class="col-md-3">
              	<select class="input-group form-control" name="sub_cat" id="sub_cat">
		                <option value="">Select One</option>
		                @foreach($categories as $ctg)
		                <option value="{{$ctg->id}}" @if($ctg->id==$ctg->sub_cat) selected="" @endif>{{$ctg->category_name}}</option>
		                @endforeach 
                </select>
	        </div>
	        <div class="col-md-2">
	        <form action="" method="post" style="display:inline-block;"> 
	        <button name="create" class="btn btn-success">Add</button>
	        </form>
	        </div>
     	</div>

  	</fieldset>
</form>
@stop
