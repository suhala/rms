@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>CATEGORY LIST</h1>
    
@stop

@section('content')
    <fieldset>
     <div class="container" style="margin-top: 15px;">
    <table class="table table-hover" align="left" width="50%">
      <thead class="thead-dark">
        <tr>
          <th>Section Details</th>
          <th></th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>ID:</td>
          <td></td>
        </tr>
        <tr>
          <td >Class Name:</td>
          <td></td>
        </tr>
        <tr>
          <td >Section Name:</td>
          <td></td>
        </tr>
        <tr>
          <td>Created time:</td>
          <td></td>
        </tr>
        <tr>
          <td>Updated time:</td>
          <td></td>
        </tr>
      </tbody>
    </table>
    <a class="btn btn-dark" href="">Back</a>
  </div>
    </fieldset>

@stop
