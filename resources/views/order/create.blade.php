@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Order Create</h1>
@stop

@section('content')

<form class="well" action="{{ route('order.store') }}" method="post">
  {{ csrf_field() }}

    <fieldset>

{{-- 
      <div class="form-group row">
        <label class="col-md-2 control-label">Order No</label>
          <div class="col-md-4">
              <input id="name" name="name" class="form-control" required="true" type="text" value="" />
          </div>
      </div> --}}

        <div class="form-group row">
          <div class="col-md-2">
            <h4><u>New Order</u></h4>
            <button type="button" class="btn btn-primary order-add">+</button>
          </div>
        </div>

      <table class="table table-sm">
        <thead>
          <tr>
            <td>#</td>
            <td>Item Name</td>
            <td>Quantity</td>
            <td>Price</td>
            {{-- <td>Subtotal</td> --}}
          </tr>
        </thead>

        <tbody class="orders"></tbody>
      </table>

      <div class="form-group">
          <center>
            <button style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Submit</button>
            <a class="btn btn-dark" href="">Back</a>
          </center>
      </div>

    </fieldset>
  </form>  
@stop

{{-- @section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop --}}

@section('js')
<script>
  $( document ).ready( function() {

    var i = 0;

    $( document ).on( 'click', '.order-remove', function() {
      $( this ).parent().parent().remove();
    } );

    $( '.order-add' ).click( function() {
      $( '.orders' ).append( '<tr class="order">\
          <td>\
            <button type="button" class="btn btn-sm btn-danger order-remove">-</button>\
          </td>\
          <td>\
             <select style="width:250px;" class="input-group form-control" name="items['+i+'][item_id]" required>\
                @foreach($items as $item)\
                  <option value="{{$item->id}}">{{$item->item_name}}</option>\
                  @endforeach\
              </select>\
          </td>\
          <td>\
            <input type="text" name="items['+i+'][qty]" placeholder="Quantity" class="form-control" required/>\
          </td>\
          <td>\
            <input type="text" name="items['+i+'][price]" placeholder="Price" class="form-control" required/>\
          </td>\
        </tr>' );
      i++;
    } );

    // $( document ).on( 'keyup', '#quantity', function() {

    //   var quantity_e = $( this );
    //   var price_e = quantity_e.parent().parent().children( 'td' ).eq( 3 ).find( '#price' );
    //   var subtotal_e = quantity_e.parent().parent().children( 'td' ).eq( 4 ).find( '#subtotal_s' );

    //   var quantity_v = parseInt( quantity_e.val() );
    //   var price_v = parseInt( price_e.val() );
    //   var subtotal_v = quantity_v * price_v;

    //   subtotal_e.text( subtotal_v );

    // } );

    // $( document ).on( 'keyup', '#price', function() {

    //   var price_e = $( this );
    //   var quantity_e = price_e.parent().parent().children( 'td' ).eq( 2 ).find( '#quantity' );
    //   var subtotal_e = price_e.parent().parent().children( 'td' ).eq( 4 ).find( '#subtotal_s' );

    //   var quantity_v = parseInt( quantity_e.val() );
    //   console.log( 'quantity ' + quantity_v );
    //   var price_v = parseInt( price_e.val() );
    //   console.log( 'price ' + price_v );
    //   var subtotal_v = quantity_v * price_v;
    //   console.log( 'subtotal ' + subtotal_v );

    //   subtotal_e.text( subtotal_v );

    // } );

  } );


</script>
@stop










{{-- @extends('adminlte::page')
@section('title', 'Test')

@section('content_header')
    <h1>Order Create</h1>
@stop

@section('content')
<form class="well" action=" {{ route( 'order.store') }} " method="post">
	{{ csrf_field() }}

    <fieldset>

    <div class="form-group row">
    	<label class="col-md-2">Order No</label>
        <div class="col-md-4">
        	<input id="order_no" name="order_no" placeholder="Order No" class="form-control" required="true" type="text"/>
        </div>

	    <label class="col-md-2">Item Name</label>
	    <div class="col-md-4">
	      	<input id="item_name" name="item_name" placeholder="Item Name" class="form-control" type="text"/>
	    </div>
	</div>
	
	<div class="form-group row">
        <label class="col-md-2">Quantity</label>
        <div class="col-md-4">
          	<input id="qty" name="qty" placeholder="Quantity" class="form-control"  type="text"/>
        </div>

        <label class="col-md-2">Price</label>
        <div class="col-md-4">
          	<input id="price" name="price" placeholder="Price" class="form-control"  type="text"/>
        </div>
	</div>

	    <div class="form-group">
      		<center>
      			<button name="create" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Add</button>
      		</center>
     	</div>
  	</fieldset>
</form>
@stop
 --}}