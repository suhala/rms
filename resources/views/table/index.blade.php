@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
     <h1>Table List</h1> 
@stop

@section('content')

    <fieldset>
     <div class="well">
    		<a href="{{ route('table.create') }}" class="btn btn-success">Create</a>

	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Floor name</td>
	    			<td>Table name</td>
	    			<td>Room no</td>
	    			<td>Action</td> 
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $tables as $table )
	    	<tbody>
	    		<td><?php echo ++$i; ?></td>
	    		<td>{{ $table->room? $table->room->floor->floor_name:""}}</td>
	    		<td>{{ $table->table_name }}</td>
	    		<td>{{$table->room? $table->room->room_name:"" }}</td>
	    		<td>
	    			<a href="{{ route('table.edit', $table->id) }}" class="btn btn-warning">Edit</a>
		              <form action="{{ route('table.destroy', $table->id) }}" method="post" style="display:inline-block;">
				          @csrf
				          @method('delete')
				          <button type="submit" class="btn btn-danger" onclick="if(confirm('Are you sure?')==0){return false;}">Delete
				          </button>
		              </form>
		        </td>
	    	</tbody>
	    	@endforeach
	    </table>
		</div>
  	</fieldset>
@stop
