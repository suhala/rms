@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')

@stop

@section('content')

<form class="well" action="{{ route('table.update',$table->id) }}" method="post">
  @csrf
  @method("put")

    <fieldset>

	    <h3>tables</h3>

      <div class="form-group row">
        <label class="col-md-2">table Name</label>
          <div class="col-md-4">
                <input id="table_name" name="table_name" placeholder="table_name" class="form-control" value="{{ $table->table_name }}" required="true" type="text"/>
          </div>
        <label class="col-md-2">Room Id</label>
          <div class="col-md-4">
            <select style="width: 330px;" class="input-group form-control" name="room_id" id="room_id" >
            <option value="">Select One</option>
            @foreach($rooms as $room)
            <option value="{{$room->id}}" @if($room->id==$table->room_id) selected="" @endif>{{$room->room_name}}</option>
            @endforeach
          </select>
          </div>
      </div>

     	<div class="form-group">
      		<center>
      			<button name="create" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Submit</button>
        		<a href="" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-primary">Back</a>
      		</center>
     	</div>

  	</fieldset>
</form>
@stop

