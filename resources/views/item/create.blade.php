@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
   <h1>Item Create</h1>
@stop

@section('content')

<form class="well" action="{{ route( 'item.store', [ $type ] ) }}" method="post" enctype="multipart/form-data">

 {{ csrf_field() }}

  <fieldset>

    <div class="form-group row">

      <label class="col-md-1">Category</label>
        <div class="col-md-3">
          <select style="width: 200px;" class="input-group form-control " name="category_id" id="category_id" >
            <option value="">Select One</option>
            @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->category_name}}</option>
            @endforeach
          </select>
        </div>

      <div>
      <label class="col-md-1">Name</label>
        <div class="col-md-3">
          <input id="item_name" name="item_name" placeholder="item_name" class="form-control" required="true" type="text"/>
        </div>
      </div>

      <label class="col-md-1">Unit</label>
        <div class="col-md-3">
          <select style="width: 200px;" class="input-group form-control" name="unit_id" id="category_id" >
            <option value="">Select One</option>
            @foreach($units as $unit)
            <option value="{{$unit->id}}">{{$unit->unit_name}}</option>
            @endforeach
          </select>
        </div>

    </div>

    <div class="form-group row">

      <label class="col-md-1">Description</label>
        <div class="col-md-3">
          <input id="description" name="description" placeholder="description" class="form-control" type="text"/>
        </div>

      <label class="col-md-1">Price</label>
        <div class="col-md-3">
          <input id="price" name="price" placeholder="price" class="form-control"  type="text"/>
        </div>

        <label class="col-md-1">Select Image</label>
          <div class="col-md-3">
                <input id="item_img" name="item_img" placeholder="" class="form-control" type="file" required />
          </div>

    </div>

    <div class="form-group">
      <center>
        <button name="create" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Add</button>
      </center>
    </div>

  </fieldset>

</form>

@stop