@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
	<h1>Item List</h1>
@stop

@section('content')

    <fieldset>

    	<div class="well">
    	<a href="{{ route('item.create', $type) }}" class="btn btn-success">Create</a>

	    <table class="table table-sm">

	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Parent Category</td>
	    			<td>Category</td>
	    			<td>Item Name</td>
	    			<td>Unit</td>
	    			<td>Quantity</td>
            		<td>Price</td>
	    			<td>Offer Price</td>
	    			<td>Action</td>
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $item as $item )
	    	<tbody class="items">
		    		<td><?php echo ++$i; ?></td>
		    		<td>{{ $item->category ? ( $item->category->parent ? $item->category->parent->category_name : '' ) : '' }}</td>
		    		<td>{{ $item->category ? $item->category->category_name : '' }}</td>
		    		<td>{{ $item->item_name }}</td>
		    		<td>{{ $item->unit ? $item->unit->unit_name : '' }}</td>
		    		<td>{{ $item->qty}}</td>
		    		<td>{{ $item->price }}</td>
		    		<td>{{ $item->offer_price }}</td>
		    		<td>
			        	<a href="{{ route('item.edit', [$type, $item->id]) }}" class="btn btn-warning">Edit</a>
			            <form action="{{ route('item.destroy',[ $type, $item->id]) }}" method="post" style="display:inline-block;">
					    @csrf
					    @method('delete')
					    <button type="submit" class="btn btn-danger" onclick="if(confirm('Are you sure?')==0){return false;}">Delete</button>
			            </form>
			        </td>

	    	</tbody>

	    	@endforeach

	    </table>

		</div>

  	</fieldset>

</form>

@stop