@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
  <h1>Item Create</h1>
@stop

@section('content')

  <form class="well" action="{{ route('item.update', [ $type, $item->id]) }}" method="post">
  @csrf
  @method("put")

  <fieldset>

    <div class="form-group row">

      <label class="col-md-1">Category</label>
      <div class="col-md-3">
        <select style="width: 200px;" class="input-group form-control" name="category_id" id="category_id" >
          <option value="">Select One</option>
          @foreach($categories as $category)
          <option value="{{$category->id}}" @if($category->id==$item->category_id) selected="" @endif>{{$category->category_name}}
          </option>
          @endforeach
        </select>
      </div>

      <div>
      <label class="col-md-1">Name</label>
        <div class="col-md-3">
        <input id="item_name" name="item_name" placeholder="item_name" class="form-control" value="{{ $item->item_name }}" required="true" type="text"/>
        </div>
      </div>

      <label class="col-md-1">Unit</label>
        <div class="col-md-3">
          <select style="width: 200px;" class="input-group form-control" name="unit_id" id="category_id" >
            <option value="">Select One</option>
            @foreach($units as $unit)
            <option value="{{$unit->id}}" @if($unit->id==$item->unit_id) selected="" @endif>{{$unit->unit_name}}</option>
            @endforeach
          </select>
        </div>

   </div>

    <div class="form-group row">

      <label class="col-md-2">Description</label>
        <div class="col-md-4">
          <input id="description" name="description" placeholder="description" class="form-control" value="{{ $item->description }}" type="text"/>
        </div>

      <label class="col-md-2">Price</label>
        <div class="col-md-4">
          <input id="price" name="price" placeholder="price" class="form-control"  value="{{ $item->price }}"type="text"/>
        </div>

    </div>

    <div class="form-group">
      <center>
        <button name="create" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Add</button>
      </center>
    </div>
  </fieldset>

</form>

@stop