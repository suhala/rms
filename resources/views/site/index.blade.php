<!DOCTYPE HTML>
<html>
<head>
<meta  charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Restaurant Management System</title>

<!-- Favicons -->

<link rel="shortcut icon" href="img/simec.png">
<link rel="apple-touch-icon" href="img/simec.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/simec.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/simec.png">

<!-- Fonts -->

<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

<!-- Styles -->

<link href="css/style.css" rel="stylesheet"  media="screen">

</head>
<body>

  <!-- Loader -->

  <div class="loader">
    <div class="loader-brand"><img style="width: 200px;" alt="" class="img-responsive center-block" src="img/brand.png"></div>
  </div>

  <div id="layout" class="layout">

    <!-- Header -->

    <header id="top" class="navbar js-navbar-affix">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#layout" class="brand js-target-scroll">
            <img class="brand-img-white" alt="" style="width: 200px;" src="img/brand-white.png">
            <img class="brand-img" alt="" width="100"  src="img/brand.png">
          </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="#layout">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#prices">Prices</a></li>
          </ul>
        </div>
      </div>
    </header>

    <!-- Home -->

    <main id="home" class="masthead masked" data-stellar-background-ratio="0.8">
      <div class="opener rel-1">
        <div class="container">
          <h1 class="wow fadeInDown">Restaurant</h1>
          <p class="lead-text">Restaurant Order Management System</p>
          <div class="control">
            <a href="#request" class="btn" data-toggle="modal">Get start now</a>
          </div>
          <div class="control wow fadeInUp" data-wow-delay="0.4s">
            <a href="https://www.youtube.com/watch?v=Hu3XUSAcK2I" class="text-white play-home js-play">
              <img alt=""  src="img/play-btn.png">
              <span>Watch Video</span>
            </a>
          </div>
        </div>
      </div>
    </main>

    <!-- Content -->

    <div class="content">

      <!-- About  -->

      <section id="about" class="about text-center section">
        <div class="container">
          <div class="row">
            <div class="text-center col-md-8 col-md-offset-2">
            <h2 class="section-title">Our Specialities</h2>
            <p class="lead-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
          </div>
          </div>
          <div class="section-body">
            <div class="row row-columns row-padding">
              <div class="column column-padding col-md-3">
                <i class="text-primary icon ion-coffee"></i>
                <h3>Breakfast</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi quae provident enim cum quidem aut corporis</p>
              </div>
              <div class="column column-padding col-md-3">
                <i class="text-primary icon ion-android-restaurant"></i>
                <h3>Dinner and Lunch</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi quae provident enim cum quidem aut corporis</p>
              </div>
              <div class="column column-padding col-md-3">
                <i class="text-primary icon ion-beer"></i>
                <h3>Beverages</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi quae provident enim cum quidem aut corporis</p>
              </div>
              <div class="column column-padding col-md-3">
                <i class="text-primary icon ion-icecream"></i>
                <h3>Ice Cream</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi quae provident enim cum quidem aut corporis</p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Prices -->

      <section id="prices" class="prices masked section" data-stellar-background-ratio="0.7">
        <div class="container rel-1">
          <div class="row-price">
            @foreach($categories as $category)
            <div class="<?php if($category->category_name == 'Main Course'){ print "leading"; }?> col-price wow flipInY">
               <div class="price-box">
                <div class="price-body">
                  <div class="price-inner">
                    <header class="price-header">
                      <h4 class="price-title">{{$category->category_name }}</h4>
                      <div class="price">
                        <strong class="price-amount"><img class="" alt="" width="100%"  src="{{$category->category_img }}"></strong>
                      </div>
                    </header>
                    <div class="price-features">
                      <ul>
                        <li style="text-align:center;"><strong>Item 1 : </strong>Price</li>
                        <li style="text-align:center;"><strong>Item 2 : </strong>Price</li>
                        <li style="text-align:center;"><strong>Item 3 : </strong>Price</li>
                      </ul>
                    </div>
                    <!-- <div class="price-footer"><a href="#request" class="btn btn-b-primary btn-block" data-toggle="modal">Select plan</a></div> -->
                    <div class="price-footer"><a href="{{route('menu.show',$category->id)}}" class="btn btn-b-gray btn-block"><img class="" alt="" width="100px;"  src="img/menu.jpg"></a></div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </section>

      <!-- Reviews -->



    </div>

    <!-- Footer -->

    <footer id="footer" class="footer text-center text-left-md bgc-light">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <div class="social">
              <a href="#" class="fa fa-facebook"></a>
              <a href="#" class="fa fa-twitter"></a>
              <a href="#" class="fa fa-pinterest"></a>
              <a href="#" class="fa fa-youtube-play"></a>
            </div>
          </div>
          <div class="col-md-7 text-right-md">
            <div class="copy">
              © 2019 Demo. All rights reserved by <a href="simecsystem.com" target="_blank">SIMEC System Ltd</a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>

  <!-- Modals -->

  <div id="request" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <span class="close" data-dismiss="modal" aria-label="Close">&times;</span>
            <h2 class="modal-title">Get start</h2>
          </div>
          <div class="modal-body text-center">
              <form class="form-request js-ajax-form">
                <div class="row-fields row">
                  <div class="form-group col-field">
                      <input type="text" class="form-control" name="name" required placeholder="Name *">
                  </div>
                   <div class="form-group col-field">
                      <input type="email" class="form-control" name="email" required placeholder="Email *">
                    </div>
                  <div class="form-group col-field">
                      <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                  </div>
                  <div class="col-sm-12">
                    <button type="submit" class="btn" data-text-hover="Submit">Send request</button>
                  </div>
                </div>
              </form>
          </div>
        </div>
    </div>
  </div>

  <!-- Modals success -->

  <div id="success" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></span>
            <h2 class="modal-title">Thank you</h2>
            <p class="modal-subtitle">Your message is successfully sent...</p>
          </div>
        </div>
    </div>
  </div>

  <!-- Modals error -->

  <div id="error" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></span>
             <h2 class="modal-title">Sorry</h2>
            <p class="modal-subtitle"> Something went wrong </p>
          </div>
        </div>
    </div>
  </div>

<!-- Scripts -->

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/interface.js"></script>
</body>
</html>
