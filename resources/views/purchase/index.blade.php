@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Requisition</h1>
@stop

@section('content')

<div class="well" action="post">

    <fieldset>

	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
            		<td>Issuer Name</td>
	    			<td>Requisition Id</td>
            		<td>Action</td>
            		<td>Status</td>
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $rawrequisitions as $rawrequisition )
	    	<tbody>
	    		<tr>
		            <td><?php echo ++$i; ?></td>
		            <td>{{ $rawrequisition->user->name }}</td>
	    			<td>{{ $rawrequisition->id }}</td>
            		<td>
		              <a href="{{route('purchase.show',$rawrequisition->id) }}" class="btn btn-info">View</a>
				    </td>

				  <td>
				  	<h4>
				  		<span class="label label-<?php if($rawrequisition->meta->formattedStatus=='Approved') {print 'success';}  elseif($rawrequisition->meta->formattedStatus=='Rejected') {print 'danger';} elseif($rawrequisition->meta->formattedStatus=='Received') {print 'primary';}elseif($rawrequisition->meta->formattedStatus=='Purchase Ordered') {print 'default';}else{print 'info';}?>">{{$rawrequisition->meta->formattedStatus}}</span>
				  	</h4>
				  </td>
	    		</tr>
	    	</tbody>
	    	@endforeach
	    </table>

  	</fieldset>
</div>
@stop

@section('js')
<script>

</script>
@stop