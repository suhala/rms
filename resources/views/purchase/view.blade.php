@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Test</h1>
@stop

@section('content')
<form action="{{ route( 'purchase.receive', $rawrequisition->id ) }}" method="post" id="form-3">
  @csrf
  @method('put')
</form>
<div class="well">
  <fieldset>

    <h3>Requisition</h3>

    <div class="form-group row">
      <label class="col-md-2 control-label">Issuer Name</label>
        <div class="col-md-4">
             {{ $rawrequisition->user->name }}
        </div>
    </div>

    <div class="form-group row">
      <label class="col-md-2 control-label">Supplier Name</label>
        <div class="col-md-4">
             {{ $rawrequisition->user->name }}
        </div>
    </div>

    <table class="table table-sm">
      <thead>
        <tr>
          <td>#</td>
          <td>Item Name</td>
          <td>Quantity</td>
          <td>Approve Quantity</td>

          @if($rawrequisition->meta->formattedStatus =='Purchase Ordered')
          <td>Receive Quantity</td>
          @endif

          <td>Price</td>

          @if(Auth::id()!= $rawrequisition->user->id)
          @if($rawrequisition->meta->formattedStatus =='Purchase Ordered')
          <td>Purchase Price</td>
          @endif
          <td>Subtotal</td>
          @endif

        </tr>
      </thead>
     @foreach( $rawrequisition->items as $i => $item )
      <tbody class="items">
        <tr class="item">
        <td>{{ $i+1 }}</td>

        <td>
          {{ $item->item->item_name}}
        </td>

        <td>
          {{ $item->qty }}
        </td>

        <td>
          {{ $item->aprv_qty }}
        </td>

        @if(Auth::id()!= $rawrequisition->user->id)
        <td>
           <input type="text" id="rcv_qty" name="rawrequisition_items[{{ $item->id }}][rcv_qty]" placeholder="Receive Quantity" class="form-control" value="{{ $item->aprv_qty }}" style="width: 150px;" form="form-3" />
        </td>
        @endif

        <td>
          {{ $item->item_price }}
        </td>

        @if(Auth::id()!= $rawrequisition->user->id)

         <td>
           <input type="text" name="rawrequisition_items[{{ $item->id }}][purchase_price]" id="purchase_price" placeholder="Purchase Price" class="form-control" value="{{ $item->item_price }}" style="width: 150px;" form="form-3" />
        </td>
        <td>
        <input type="text" name="rawrequisition_items[{{ $item->id }}][subtotal_purchase]" id="subtotal" value="{{ $item->subtotal }}" form="form-3" required readonly />
      </td>
        @endif

      </tr>
      </tbody>
      @endforeach
        <tfoot>
          <tr>

            <td colspan="6">&nbsp;</td>
            <td align="right">Total</td>
            <td align="right"><input type="text" name="total_purchase" id="total" value="{{$item->total}}" placeholder="Total" class="form-control"  form="form-3" required readonly/></td>
          </tr>
        </tfoot>
    </table>

    <div class="form-group">
        <center>

          @if(Auth::id()!= $rawrequisition->user->id && $rawrequisition->meta->formattedStatus =='Purchase Ordered')
          <button style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" href="{{ route( 'purchase.receive', $rawrequisition->id ) }}" class="btn btn-danger" form="form-3">Receive</button>
        @endif

          <a href="{{ route('purchase.index') }}" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-primary">Back</a>
        </center>
    </div>

  </fieldset>
</div>
@endsection

@section( 'js' )
<script>
  var total=0;
$( document ).on( 'keyup', '#rcv_qty', function() {
  /*var quantity_e = $( this );
  var price_e = quantity_e.parent().next().next().find( '#purchase_price' );
  var subtotal_e = price_e.parent().next().find( '#subtotal' );
  var price_v = price_e.val();
  var quantity_v = quantity_e.val();
  var subtotal_v = price_v * quantity_v;
  subtotal_e.val( subtotal_v );
  console.log(subtotal_v);*/
  $(this).parent().next().next().find('#purchase_price').trigger( 'keyup' );
} );

$( document ).on( 'keyup', '#purchase_price', function() {
  var price_e = $( this );
  var quantity_e = price_e.parent().prev().prev().find( '#rcv_qty' );
  var subtotal_e = price_e.parent().next().find( '#subtotal' );
  var price_v = price_e.val();
  var quantity_v = quantity_e.val();
  var subtotal_ov= subtotal_e.val();
  var subtotal_nv = price_v * quantity_v;
  subtotal_e.val( subtotal_nv );

  total += Number(subtotal_nv)-Number(subtotal_ov);
   $('#total').val(total);
} );
</script>
@endsection
