@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>OFFER LIST</h1>
@stop

@section('content')
    <fieldset>
     <div class="well">
    		<a href="{{ route('offer.create') }}" class="btn btn-success">Create</a>

	    <table class="table table-sm">
	    	<thead>
	    		<tr style="font-weight: bold;" >
	    			<td>#</td>
	    			<td>Item Name</td>
	    			<td>Price</td>
	    			<td>Discount Rate</td>
	    			<td>Discount Price</td>
	    			<td>Date</td>
	    			<td>Duration</td>
	    			<td>Action</td>
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $offers as $offer )
	    	<tbody>
	    			<td><?php echo ++$i; ?></td>
		    		<td>{{ $offer->item_name }}</td>
		    		<td>{{ $offer->price }}</td>
		    		<td>{{ $offer->discount_rate }}</td>
		    		<td>{{ $offer->discount_price }}</td>
		    		<td>{{ $offer->date }}</td>
		    		<td>{{ $offer->duration }}</td>
		    		<td>
		           <a href="{{ route('offer.edit', $offer->id) }}" class="btn btn-warning">Edit</a>
		              <form action="{{ route('offer.destroy', $offer->id) }}" method="post" style="display:inline-block;">
				          @csrf
				          @method('delete')
				          <button type="submit" class="btn btn-danger" onclick="if(confirm('Are you sure?')==0){return false;}">Delete
				          </button>
		              </form>
			        </td>
	    	</tbody>
			@endforeach
	    </table>
	</div>
  	</fieldset>

@stop
