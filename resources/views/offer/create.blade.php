@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>OFFER ITEM</h1>
@stop

@section('content')
<form class="well" action="{{ route('offer.store') }}" method="post">
	{{ csrf_field() }}

    <fieldset>

	  <h3>offer</h3>

	    <div class="form-group row">
	    	<label class="col-md-2 control-label">Item Name</label>
	        <div class="col-md-4">
              	{{-- <input id="item_name" name="item_name" placeholder="Item Name" class="form-control" required="true" type="text"/>  --}}
               <select class="input-group form-control" name="item_name" id="item_name" >
	              	<option value="">Select One</option>
	              	 @foreach($items as $item)
		              	<option value="{{$item->id}}">{{$item->item_name}}</option>
	              	@endforeach 
	            </select>  
	    </div>
	</div>
	    <div class="form-group row">
	    	<label class="col-md-2 control-label">Price</label>
	        <div class="col-md-4">
              	<input id="price" name="price" placeholder="Price" class="form-control" required="true" type="text"/>
	        </div>
	    </div>
	    
	    <div class="form-group row">
	        <label class="col-md-2 control-label">Discount Rate</label>
	        <div class="col-md-4">
              	<input id="discount_rate" name="discount_rate" placeholder="Discount Rate" class="form-control" required="true" type="text"/>
	        </div>
	    </div>

	    <div class="form-group row">
	        <label class="col-md-2 control-label">Discount Price</label>
	        <div class="col-md-4">
              	<input id="discount_price" name="discount_price" placeholder="Discount Price" class="form-control" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group row">
	        <label class="col-md-2 control-label">Valid Date</label>
	        <div class="col-md-4">
              	<input id="date" name="date" placeholder="date" class="form-control" required="true" type="date" />
	        </div>
	    </div>
	    <div class="form-group row">
	        <label class="col-md-2 control-label">Duration</label>
	        <div class="col-md-4">
              	<input id="duration" name="duration" placeholder="Duration" class="form-control" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group">
      		<center>
      			<button name="create" class="btn btn-success">Add</button>
      		</center>
     	</dsiv>
     	</div>

  	</fieldset>
</form>
@stop
