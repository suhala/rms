@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Updating A Offer</h1>
@stop

@section('content')
<form class="well" action="{{ route('offer.update',$offer->id) }}" method="post">
	@csrf
    @method("put")

    <fieldset>

	    <div class="form-group row">
	    	<label class="col-md-2 control-label">Item Name</label>
	        <div class="col-md-4">
              	<input id="item_name" name="item_name" placeholder="Item Name" class="form-control" value="{{ $offer->item_name }}" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group row">
	    	<label class="col-md-2 control-label">Price</label>
	        <div class="col-md-4">
              	<input id="price" name="price" placeholder="Price" class="form-control" value="{{ $offer->price }}" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group row">
	        <label class="col-md-2 control-label">Discount Rate</label>
	        <div class="col-md-4">
              	<input id="discount_rate" name="discount_rate" placeholder="Discount Rate" class="form-control" value="{{ $offer->discount_rate }}"required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group row">
	        <label class="col-md-2 control-label">Discount Price</label>
	        <div class="col-md-4">
              	<input id="discount_price" name="discount_price" placeholder="Discount Price" class="form-control" value="{{ $offer->discount_price }}" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group row">
	        <label class="col-md-2 control-label">Valid Date</label>
	        <div class="col-md-4">
              	<input id="date" name="date" placeholder="date" class="form-control" value="{{ $offer->date }}" required="true" type="date"/>
	        </div>
	    </div>
	    <div class="form-group row">
	        <label class="col-md-2 control-label">Duration</label>
	        <div class="col-md-4">
              	<input id="duration" name="duration" placeholder="Duration" class="form-control" value="{{ $offer->duration }}" required="true" type="text"/>
	        </div>
	    </div>
	        <div class="col-md-2">
	        <form action="" method="post" style="display:inline-block;"> 
	        <button name="create" class="btn btn-success">Update</button>
	        </form>
	        </div>
     	</div>

  	</fieldset>
</form>
@stop
