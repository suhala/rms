@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Supplier</h1>
@stop

@section('content')
<form class="well" action="{{ route('supplier.store') }}" method="post">
	{{ csrf_field() }}

    <fieldset>

	    <div class="form-group row">
	    	<label class="col-md-2 control-label">Supplier Name</label>
	        <div class="col-md-4">
              	<input id="name" name="name" placeholder="Supplier Name" class="form-control" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group row">
	    	<label class="col-md-2 control-label">Address</label>
	        <div class="col-md-4">
              	<input id="address" name="address" placeholder="Address" class="form-control" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group row">
	        <label class="col-md-2 control-label">Contact No</label>
	        <div class="col-md-4">
              	<input id="contact" name="contact" placeholder="Contact No" class="form-control" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group row">
	        <label class="col-md-2 control-label">Email</label>
	        <div class="col-md-4">
              	<input id="email" name="email" placeholder="Email" class="form-control" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group">
      		<center>
      			<button name="create" class="btn btn-success">Add</button>
      		</center>
     	</dsiv>
     	</div>

  	</fieldset>
</form>
@stop
