@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Supplier List</h1>
@stop

@section('content')
    <fieldset>
     <div class="well">
    		<a href="{{ route('supplier.create') }}" class="btn btn-success">Create</a>

	    <table class="table table-sm">
	    	<thead>
	    		<tr style="font-weight: bold;" >
	    			<td>#</td>
	    			<td>Name</td>
	    			<td>Address</td>
	    			<td>Contact No</td>
	    			<td>Email</td>
	    			<td>Action</td>
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $suppliers as $supplier )
	    	<tbody>
	    			<td><?php echo ++$i; ?></td>
		    		<td>{{ $supplier->name }}</td>
		    		<td>{{ $supplier->address }}</td>
		    		<td>{{ $supplier->contact }}</td>
		    		<td>{{ $supplier->email }}</td>
		    		<td>
		           <a href="{{ route('supplier.edit', $supplier->id) }}" class="btn btn-warning">Edit</a>
		              <form action="{{ route('supplier.destroy', $supplier->id) }}" method="post" style="display:inline-block;">
				          @csrf
				          @method('delete')
				          <button type="submit" class="btn btn-danger" onclick="if(confirm('Are you sure?')==0){return false;}">Delete
				          </button>
		              </form>
			        </td>
	    	</tbody>
			@endforeach
	    </table>
	</div>
  	</fieldset>

@stop
