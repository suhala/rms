@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Updating A Supplier</h1>
@stop

@section('content')
<form class="well" action="{{ route('supplier.update',$supplier->id) }}" method="post">
	@csrf
    @method("put")

    <fieldset>

	        <div class="form-group row">
	    	<label class="col-md-2 control-label">Supplier Name</label>
	        <div class="col-md-4">
              	<input id="name" name="name" placeholder="Supplier Name" class="form-control" value="{{ $supplier->name }}" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group row">
	    	<label class="col-md-2 control-label">Address</label>
	        <div class="col-md-4">
              	<input id="address" name="address" placeholder="Address" class="form-control" value="{{ $supplier->address }}" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group row">
	        <label class="col-md-2 control-label">Contact No</label>
	        <div class="col-md-4">
              	<input id="contact" name="contact" placeholder="Contact No" class="form-control" value="{{ $supplier->contact }}" required="true" type="text"/>
	        </div>
	    </div>
	    <div class="form-group row">
	        <label class="col-md-2 control-label">Email</label>
	        <div class="col-md-4">
              	<input id="email" name="email" placeholder="Email" class="form-control" value="{{ $supplier->email }}" required="true" type="text"/>
	        </div>
	    </div>
	        <div class="col-md-2">
	        <form action="" method="post" style="display:inline-block;"> 
	        <button name="create" class="btn btn-success">Update</button>
	        </form>
	        </div>
     	</div>

  	</fieldset>
</form>
@stop
