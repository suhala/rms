@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
     <h1>Unit List</h1> 
@stop

@section('content')

    <fieldset>
     <div class="well">
    		<a href="{{ route('unit.create') }}" class="btn btn-success">Create</a>

	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Unit name</td>
	    			<td>Action</td> 
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $units as $unit )
	    	<tbody>
	    		<td><?php echo ++$i; ?></td>
	    		<td>{{ $unit->unit_name }}</td>
	    		<td>
	    			<a href="{{ route('unit.edit', $unit->id) }}" class="btn btn-warning">Edit</a>
		              <form action="{{ route('unit.destroy', $unit->id) }}" method="post" style="display:inline-block;">
				          @csrf
				          @method('delete')
				          <button type="submit" class="btn btn-danger" onclick="if(confirm('Are you sure?')==0){return false;}">Delete
				          </button>
		              </form>
		        </td>
	    	</tbody>
	    	@endforeach
	    </table>
		</div>
  	</fieldset>
@stop
