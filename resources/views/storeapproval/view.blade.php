@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
    <h1>Test</h1>
@stop

@section('content')
<form action="{{ route( 'storeapproval.receive', $finrequisition->id ) }}" method="post" id="form-3">
  @csrf
  @method('put')
</form>
<div class="well">
  <fieldset>

    <h3>Requisition</h3>

    <div class="form-group row">
      <label class="col-md-2 control-label">Issuer Name</label>
        <div class="col-md-4">
             {{ $finrequisition->user->name }}
        </div>
    </div>

    <div class="form-group row">
      <label class="col-md-2 control-label">Supplier Name</label>
        <div class="col-md-4">
             {{ $finrequisition->user->name }}
        </div>
    </div>

    <table class="table table-sm">
      <thead>
        <tr>
          <td>#</td>
          <td>Item Name</td>
          <td style=" margin-right:200px;">Quantity</td>
          <td style=" margin-right:200px;">Approve Quantity</td>

          @if($finrequisition->meta->formattedStatus =='Material Sent')
          <td style=" margin-right:200px;">Receive Quantity</td>
          @endif

          {{-- <td style=" margin-right:200px;">Price</td> --}}

          {{-- @if(Auth::id()!= $finrequisition->user->id)
          @if($finrequisition->meta->formattedStatus =='Sale Ordered')
          <td style=" margin-right:200px;">Purchase Price</td>
          @endif
          @endif --}}

        </tr>
      </thead>
     @foreach( $finrequisition->items as $i => $item )
      <tbody class="items">
        <tr class="item">
        <td>{{ $i+1 }}</td>

        <td>
          {{ $item->item->item_name}}
        </td>

        <td>
          {{ $item->qty }}
        </td>

        <td>
          {{ $item->aprv_qty }}
        </td>

        @if(Auth::id()!= $finrequisition->user->id && $finrequisition->meta->formattedStatus =='Material Sent')
        <td>
           <input type="text" name="finrequisition_items[{{ $item->id }}][rcv_qty]" placeholder="Receive Quantity" class="form-control" value="{{ $item->aprv_qty }}" style="width: 150px;" form="form-3" />
        </td>
        @endif

        {{-- <td>
          {{ $item->item_price }}
        </td> --}}

        {{-- @if(Auth::id()!= $finrequisition->user->id)
         <td>
           <input type="text" name="finrequisition_items[{{ $item->id }}][finpurchase_price]" placeholder="finpurchase Price" class="form-control" value="{{ $item->item_price }}" style="width: 150px;" form="form-3" />
        </td>
        @endif --}}

      </tr>
      </tbody>
      @endforeach
    </table>

    <div class="form-group">
        <center>

          @if(Auth::id()!= $finrequisition->user->id && $finrequisition->meta->formattedStatus =='Material Sent')
          <button style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" href="{{ route( 'storeapproval.receive', $finrequisition->id ) }}" class="btn btn-danger" form="form-3">Receive</button>
        @endif

          <a href="{{ route('storeapproval.index') }}" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-primary">Back</a>
        </center>
    </div>

  </fieldset>
</div>
@stop
