@extends('adminlte::page')
@section('title', 'Test')

@section('content_header')
    <h1>Todays Order</h1>
@stop

@section('content')

    <fieldset>
    	<div class="well">
    		<a href="{{ route('store.create') }}" class="btn btn-success">Create</a>
	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Order No</td>
	    			<td>Item Name</td>
	    			<td>Quantity</td>
            		<td>Price</td>
	    			<td>Action</td> 
	    		</tr>
	    	</thead>
	    	
	    	<tbody class="orders">
		    		<td></td>
		    		<td></td>
		    		<td></td>
		    		<td></td>
		    		<td></td>
		    		<td>
			        <a href="" class="btn btn-warning">Edit</a>
			              <form action="{{ route('order.destroy', $order->id) }}" method="post" style="display:inline-block;">
					          @csrf
					          @method('delete')
					          <button type="submit" class="btn btn-danger" onclick="if(confirm('Are you sure?')==0){return false;}">Delete</button>
			              </form>
			        </td>
	    	</tbody>
			@endforeach
	    </table>
		</div>
  	</fieldset>
</form>
@stop
