@extends('adminlte::page')
@section('title', 'Test')

@section('content_header')
    <h1>Edit Order</h1>
@stop

@section('content')
<form class="well" action="{{ route('order.update',$order->id) }}" method="post">
	@csrf
    @method("put")

    <fieldset>
	    
    <div class="form-group row">
    	<label class="col-md-2">Order No</label>
        <div class="col-md-4">
        	<input id="order_no" name="order_no" placeholder="Order No" class="form-control" value="{{ $order->order_no }}" required="true" type="text"/>
        </div>

	    <label class="col-md-2">Item Name</label>
	    <div class="col-md-4">
	      	<input id="item_name" name="item_name" placeholder="Item Name" value="{{ $order->item_name }}" class="form-control" type="text"/>
	    </div>
	</div>
	
	<div class="form-group row">
        <label class="col-md-2">Quantity</label>
        <div class="col-md-4">
          	<input id="qty" name="qty" placeholder="Quantity" class="form-control" value="{{ $order->qty }}" type="text"/>
        </div>

        <label class="col-md-2">Price</label>
        <div class="col-md-4">
          	<input id="price" name="price" placeholder="Price" class="form-control" value="{{ $order->price }}" type="text"/>
        </div>
	</div>

	    <div class="form-group">
      		<center>
      			<button name="create" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Update</button>
      		</center>
     	</div>
  	</fieldset>
</form>
@stop
