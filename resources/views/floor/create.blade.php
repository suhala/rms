@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')

@stop

@section('content')

<form class="well" action="{{ route('floor.store') }}" method="post">
	{{ csrf_field() }}

    <fieldset>

	    <h3>floors</h3>

      <div class="form-group row">
        <label class="col-md-2">Floor Name</label>
          <div class="col-md-4">
                <input id="floor_name" name="floor_name" placeholder="floor_name" class="form-control" required="true" type="text"/>
          </div>
      </div>

     	<div class="form-group">
      		<center>
      			<button name="create" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Submit</button>
        		{{-- <a href="" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-primary">Back</a> --}}
      		</center>
     	</div>

  	</fieldset>
</form>
@stop
