@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
     <h1>Floor List</h1> 
@stop

@section('content')

    <fieldset>
     <div class="well">
    		<a href="{{ route('floor.create') }}" class="btn btn-success">Create</a>

	    <table class="table table-sm">
	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Floor name</td>
	    			<td>Action</td> 
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $floors as $floor )
	    	<tbody>
	    		<td><?php echo ++$i; ?></td>
	    		<td>{{ $floor->floor_name }}</td>
	    		<td>
	    			<a href="{{ route('floor.edit', $floor->id) }}" class="btn btn-warning">Edit</a>
		              <form action="{{ route('floor.destroy', $floor->id) }}" method="post" style="display:inline-block;">
				          @csrf
				          @method('delete')
				          <button type="submit" class="btn btn-danger" onclick="if(confirm('Are you sure?')==0){return false;}">Delete
				          </button>
		              </form>
		        </td>
	    	</tbody>
	    	@endforeach
	    </table>
		</div>
  	</fieldset>
@stop
