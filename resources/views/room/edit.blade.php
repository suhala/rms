@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
  <h1>Room Create</h1>
@stop

@section('content')

  <form class="well" action="{{ route('room.update', $room->id) }}" method="post">
  @csrf
  @method("put")

  <fieldset>

    <div class="form-group row">

      <label class="col-md-2">Floor</label>
      <div class="col-md-4">
        <select style="width: 330px;" class="input-group form-control" name="floor_id" id="floor_id" >
          <option value="">Select One</option>
          @foreach($floors as $floor)
          <option value="{{$floor->id}}" @if($floor->id==$room->floor_id) selected="" @endif>{{$floor->floor_name}}
          </option>
          @endforeach
        </select>
      </div>

      <label class="col-md-2">Name</label>
        <div class="col-md-4">
        <input id="room_name" name="room_name" placeholder="room_name" class="form-control" value="{{ $room->room_name }}" required="true" type="text"/>
        </div>
      </div>


    <div class="form-group">
      <center>
        <button name="create" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Add</button>
      </center>
    </div>
  </fieldset>

</form>

@stop