@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
	<h1>Room List</h1>
@stop

@section('content')

    <fieldset>

    	<div class="well">
    	<a href="{{ route('room.create') }}" class="btn btn-success">Create</a>

	    <table class="table table-sm">

	    	<thead>
	    		<tr>
	    			<td>#</td>
	    			<td>Room Name</td>
	    			<td>Floor Name</td>
	    			<td>Action</td>
	    		</tr>
	    	</thead>
	    	<?php $i=0; ?>
	    	@foreach( $rooms as $room )
	    	<tbody>
		    		<td><?php echo ++$i; ?></td>
		    		<td>{{ $room->room_name }}</td>
		    		<td>{{ $room->floor->floor_name}}</td>
		    		<td>
			        	<a href="{{ route('room.edit', $room->id) }}" class="btn btn-warning">Edit</a>
			            <form action="{{ route('room.destroy', $room->id) }}" method="post" style="display:inline-block;">
					    @csrf
					    @method('delete')
					    <button type="submit" class="btn btn-danger" onclick="if(confirm('Are you sure?')==0){return false;}">Delete</button>
			            </form>
			        </td>

	    	</tbody>

	    	@endforeach

	    </table>

		</div>

  	</fieldset>

</form>

@stop