@extends('adminlte::page')

@section('title', 'Test')

@section('content_header')
   <h1>Room Create</h1>
@stop

@section('content')

<form class="well" action="{{ route( 'room.store') }}" method="post">

 {{ csrf_field() }}

  <fieldset>

    <div class="form-group row">
    <label class="col-md-2">Floor ID</label>
        <div class="col-md-4">
          <select style="width: 330px;" class="input-group form-control" name="floor_id" id="floor_id" >
            <option value="">Select One</option>
            @foreach($floors as $floor)
            <option value="{{$floor->id}}">{{$floor->floor_name}}</option>
            @endforeach
          </select>
        </div>

        <label class="col-md-2">Name</label>
        <div class="col-md-4">
          <input id="room_name" name="room_name" placeholder="room_name" class="form-control" required="true" type="text"/>
        </div>
      </div>


    <div class="form-group">
      <center>
        <button name="create" style=" margin-top: 10px; padding-left: 80px; padding-right: 80px;" class="btn btn-success">Add</button>
      </center>
    </div>

  </fieldset>

</form>

@stop