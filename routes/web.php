<?php

/*\DB::listen( function( $sql ) {
    \Log::info( $sql->sql );
    \Log::info( $sql->bindings );
    \Log::info( $sql->time );
} );*/

// Route::get( '/test', 'HomeController@test' );

Route::group(['middleware'=>'auth'],function(){
Route::group(['prefix'=>'/category/{type}','where'=>['type'=>'1|2']],function(){
	Route::get( '/', 'CategoryController@index' )->name('category.index');
	Route::get( '/create', 'CategoryController@create' )->name('category.create');
	Route::post( '/', 'CategoryController@store' )->name('category.store');
	Route::get( '/{category}/show', 'CategoryController@show' )->name('category.show');
	Route::get( '/{category}/edit', 'CategoryController@edit' )->name('category.edit');
	Route::put( '/{category}', 'CategoryController@update' )->name('category.update');
	Route::delete( '/{category}', 'CategoryController@destroy' )->name('category.destroy');
});

Route::resource( '/offer', 'OfferController' );

Route::group(['prefix'=>'/item/{type}','where'=>[ 'type'=>'1|2' ]],function() {
	Route::get( '/', 'ItemController@index' )->name('item.index');
	Route::get( '/create', 'ItemController@create' )->name('item.create');
	Route::post( '/', 'ItemController@store' )->name('item.store');
	Route::get( '/{item}/edit', 'ItemController@edit' )->name('item.edit');
	Route::put( '/{item}', 'ItemController@update' )->name('item.update');
	Route::delete( '/{item}', 'ItemController@destroy' )->name('item.destroy');
});


Route::group(['namespace'=>'RawMaterials'],function() {

Route::group(['prefix'=>'/rawrequisition'],function() {
	Route::get( '/', 'RawrequisitionController@index' )->name('rawrequisition.index');
	Route::get( '/create', 'RawrequisitionController@create' )->name('rawrequisition.create');
	Route::post( '/', 'RawrequisitionController@store' )->name('rawrequisition.store');
	Route::get( '/{rawrequisition}/show', 'RawrequisitionController@show' )->name('rawrequisition.show');
	Route::get( '/{rawrequisition}/edit', 'RawrequisitionController@edit' )->name('rawrequisition.edit');
	Route::put( '/update/{rawrequisition}', 'RawrequisitionController@update' )->name('rawrequisition.update');
	Route::post( '/approval/{rawrequisition}', 'RawrequisitionController@approve' )->name('rawrequisition.approve');
	Route::post( '/approval/{rawrequisition}', 'RawrequisitionController@reject' )->name('rawrequisition.reject');
	Route::delete( '/{rawrequisition}', 'RawrequisitionController@destroy' )->name('rawrequisition.destroy');
	Route::put( '/approve/{rawrequisition}', 'RawrequisitionController@approve' )->name('rawrequisition.approve');
	Route::put( '/reject/{rawrequisition}', 'RawrequisitionController@reject' )->name('rawrequisition.reject');
});


Route::group(['prefix'=>'/approval'],function() {
	Route::get( '/', 'ApprovalController@index' )->name('approval.index');
	Route::get( '/{rawrequisition}/show', 'ApprovalController@show' )->name('approval.show');
	Route::put( '/purchase/{rawrequisition}', 'ApprovalController@purchase' )->name('approval.purchase');
});


Route::group(['prefix'=>'/purchase'],function() {
	Route::get( '/', 'PurchaseController@index' )->name('purchase.index');
	Route::get( '/{rawrequisition}/show', 'PurchaseController@show' )->name('purchase.show');
	Route::put( '/purchase/{rawrequisition}', 'PurchaseController@receive' )->name('purchase.receive');
});


Route::group(['prefix'=>'/receive'],function() {
	Route::get( '/', 'ReceiveController@index' )->name('receive.index');
	Route::get( '/{rawrequisition}/show', 'ReceiveController@show' )->name('receive.show');
});

});


Route::group(['namespace'=>'FinishedGoods'],function() {

Route::group(['prefix'=>'/finrequisition'],function() {
	Route::get( '/', 'FinrequisitionController@index' )->name('finrequisition.index');
	Route::get( '/create', 'FinrequisitionController@create' )->name('finrequisition.create');
	Route::post( '/', 'FinrequisitionController@store' )->name('finrequisition.store');
	Route::get( '/{finrequisition}/show', 'FinrequisitionController@show' )->name('finrequisition.show');
	Route::get( '/{finrequisition}/edit', 'FinrequisitionController@edit' )->name('finrequisition.edit');
	Route::put( '/update/{finrequisition}', 'FinrequisitionController@update' )->name('finrequisition.update');
	Route::post( '/approval/{finrequisition}', 'FinrequisitionController@approve' )->name('finrequisition.approve');
	Route::post( '/approval/{finrequisition}', 'FinrequisitionController@reject' )->name('finrequisition.reject');
	Route::delete( '/{finrequisition}', 'FinrequisitionController@destroy' )->name('finrequisition.destroy');
	Route::put( '/approve/{finrequisition}', 'FinrequisitionController@approve' )->name('finrequisition.approve');
	Route::put( '/reject/{finrequisition}', 'FinrequisitionController@reject' )->name('finrequisition.reject');
});

Route::group(['prefix'=>'/finapproval'],function() {
	Route::get( '/', 'FinapprovalController@index' )->name('finapproval.index');
	Route::get( '/{finrequisition}/show', 'FinapprovalController@show' )->name('finapproval.show');
	Route::post( '/{finrequisition}/send', 'FinapprovalController@send' )->name('finapproval.send');
});

Route::group(['prefix'=>'/storeapproval'],function() {
	Route::get( '/', 'StoreapprovalController@index' )->name('storeapproval.index');
	Route::get( '/{finrequisition}/show', 'StoreapprovalController@show' )->name('storeapproval.show');
	Route::put( '/finpurchase/{finrequisition}', 'StoreapprovalController@receive' )->name('storeapproval.receive');
});


Route::group(['prefix'=>'/fgreceive'],function() {
	Route::get( '/', 'FgreceiveController@index' )->name('fgreceive.index');
	Route::get( '/{finrequisition}/show', 'FgreceiveController@show' )->name('fgreceive.show');
	Route::get( '/purchase/{finrequisition}', 'FgreceiveController@purchase' )->name('fgreceive.purchase');
});

});

//end


Route::resource( '/unit', 'UnitController' );

Route::resource( '/floor', 'FloorController' );

Route::resource( '/room', 'RoomController' );

Route::resource( '/table', 'TableController' );

Route::resource( '/cusorder', 'CusOrderController' );
Route::get( '/cusorder-item-id', 'CusOrderController@getPrice' )->name('cusordergetPrice');

Route::resource( '/formula', 'FormulaController' );

Route::resource( '/supplier', 'SupplierController' );



Route::get('/', function () {
    return view('welcome');
});
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/website', 'SiteController@index')->name('website');

Route::get( '/menu/{category_id}', 'MenuController@show' )->name('menu.show');