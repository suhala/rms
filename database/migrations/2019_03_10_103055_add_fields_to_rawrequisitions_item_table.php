<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToRawrequisitionsItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawrequisitions_item', function (Blueprint $table) {
            $table->double('purchase_price')->nullable()->after('item_price');
            // $table->integer('supplier_id')->nullable()->after('item_id');
            $table->integer('supplier_id')->nullable();
            $table->integer('pur_supplier_id')->nullable()->after('supplier_id');
            $table->double('req_type')->nullable()->after('item_price');
            $table->double('subtotal')->nullable()->after('purchase_price');
            $table->double('subtotal_purchase')->nullable()->after('subtotal');
            $table->double('total')->nullable()->after('subtotal_purchase');
            $table->double('total_purchase')->nullable()->after('total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawrequisitions_item', function (Blueprint $table) {
            //
        });
    }
}
