<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinrequisitionsItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finrequisitions_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('finrequisition_id')->nullable();;
            $table->integer('item_id')->nullable();
            $table->double('item_price')->nullable();
            $table->integer('qty')->nullable();
            $table->integer('aprv_qty')->nullable();
            $table->integer('rcv_qty')->nullable();
            $table->double('purchase_price')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->integer('pur_supplier_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finrequisitions_item');
    }
}
