<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalToRawrequisitions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawrequisitions', function (Blueprint $table) {
            $table->double('total')->nullable()->after('supplier_id');
            $table->double('total_purchase')->nullable()->after('total');
                    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawrequisitions', function (Blueprint $table) {
            //
        });
    }
}
