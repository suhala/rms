<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCusorderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cusorder', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('table_id')->nullable();
            $table->float('total')->nullable();
            $table->float('discount')->nullable();
            $table->float('vat')->nullable();
            $table->float('due')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cusorder');
    }
}
