<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawrequisitionsItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rawrequisitions_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rawrequisition_id');
            $table->double('item_price');
            $table->integer('qty');
            $table->integer('aprv_qty');
            $table->integer('rcv_qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rawrequisitions_item');
    }
}
