<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormulaItem extends Model
{
    protected $table = 'formulas_item';
    protected $fillable = [
         'item_id','formula_id','amount'
    ];

    protected $hidden = [

    ];


    public function raw_item()
    {
        // return $this->hasMany('App\Item', 'id', 'item_id' );
        // return $this->hasOne('App\Item', 'id', 'item_id' );
        return $this->belongsTo( 'App\Item', 'item_id', 'id' );
    }
}