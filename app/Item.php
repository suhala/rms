<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    protected $fillable = [
         'item_name', 'category_id', 'price', 'description', 'offer_price', 'item_type','qty','unit_id','item_img'
    ];


    protected $hidden = [

    ];

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }


   public function unit()
    {
        return $this->belongsTo('App\Unit');
    }


   public function formula()
    {
        // return $this->hasMany('App\Formula');
        return $this->hasOne( 'App\Formula' );
    }

    //  public function formula_item()
    // {
    //     return $this->hasOne( 'App\Formula');
    // }

}
