<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $table = 'order_tables';
    protected $fillable = [
    'table_name','room_id','status'
    ];

    protected $hidden = [

    ];

    public function room()
    {
    	return $this->belongsTo( 'App\Room' );
    }
}
