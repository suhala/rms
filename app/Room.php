<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';
    protected $fillable = [
         'room_name', 'floor_id'
    ];


    protected $hidden = [

    ];

    public function floor()
    {
    	return $this->belongsTo('App\Floor');
    }

    public function tables()
    {
        return $this->hasMany( 'App\Table');
    }
}
