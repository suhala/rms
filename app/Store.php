<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'stores';
    protected $fillable = [
         'order_no','item_name', 'qty', 'price'
    ];
    
    // public function category()
    // {
    // 	return $this->belongsTo('App\Category');
    // }
}
