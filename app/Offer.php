<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $table = 'offers';
    protected $fillable = [
         // 'category_name', 'sub_cat', 'cat_type'
         'item_name', 'price', 'discount_rate', 'discount_price', 'date', 'duration'
    ];

     public function item()
     {
     	return $this->belongsTo( 'App\Item');
     }
}
