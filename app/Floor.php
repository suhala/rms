<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $table = 'floors';
    protected $fillable = [
         'floor_name'
    ];

    protected $hidden = [

    ];

    public function rooms()
    {
    	return $this->hasMany( 'App\Room');
    }
}
