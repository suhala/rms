<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cusorder extends Model
{
    protected $table = 'cusorder';
    protected $fillable = [ 'table_id', 'total', 'discount', 'vat', 'due', 'status' ];

    public function items()
    {
        return $this->hasMany('App\CusorderDetails');
    }

    public function table()
    {
        return $this->belongsTo('App\Table');
    }

    public function getMetaAttribute()
    {
        $data = (object) [
            'formattedStatus' => 'Pending',
            'isApproved' => false,
        ];
        if($this->status==1){
            $data->formattedStatus='Approved';
        } elseif($this->status==2){
            $data->formattedStatus='Received';
        } elseif($this->status==3){
            $data->formattedStatus='Rejected';
        } elseif($this->status==4){
            $data->formattedStatus='Purchase Ordered';
        }

        if($this->status==1){
            $data->isApproved=true;
        }
        return $data;
    }
}
