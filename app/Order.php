<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    // protected $fillable = [ 'order_no', 'item_name', 'qty', 'price'];
    
     public function items()
    {
    	return $this->hasMany('App\OrderItem', 'order_id', 'id');
    }
}
