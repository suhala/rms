<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CusorderDetails extends Model
{

    protected $table = 'cusorder_details';
    protected $fillable = [ 'item_id', 'cusorder_id', 'quantity', 'subtotal', 'discount' ];

     public function item()
    {
    	return $this->belongsTo('App\Item');
    }
}
