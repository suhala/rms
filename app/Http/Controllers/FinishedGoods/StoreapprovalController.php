<?php

namespace App\Http\Controllers\FinishedGoods;

use App\Http\Controllers\Controller;

use App\Models\FinishedGoods\Finrequisition;
use App\Models\FinishedGoods\Sale;
use App\Models\FinishedGoods\FinrequisitionItem;
use App\Item;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class StoreapprovalController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index()
    {
        $items = Item::all()->where('item_type', '1');
        $finrequisition = Finrequisition::with('user')->latest()->where('status','4')->get();
        return view( 'storeapproval.index', ['items'=>$items, 'finrequisitions'=>$finrequisition] );
    }

    public function show(Finrequisition $finrequisition)
    {
        $finrequisition->load('items.item','user');//lazy load
        // dd($finrequisition);
        return view('storeapproval.view',['finrequisition'=>$finrequisition]);
    }


 public function receive( Request $request, Finrequisition $finrequisition )
    {
        // dd( $request->all() );
        $finrequisition->load( 'items' );
        $finrequisition->items->each( function( $item ) use ( $request ) {
            $finrequisitionItems = [];
            if ( $request->has( 'finrequisition_items' ) ) {
                $finrequisitionItems = $request->input( 'finrequisition_items' );
            }
            if ( isset( $finrequisitionItems[$item->id]['purchase_price'] ) ) {
                $item->item_price = $finrequisitionItems[$item->id]['purchase_price'];
            } else {
                $item->item_price = $item->purchase_price;
            }
            if ( isset( $finrequisitionItems[$item->id]['rcv_qty'] ) ) {
                $item->rcv_qty = $finrequisitionItems[$item->id]['rcv_qty'];
            } else {
                $item->rcv_qty = $item->aprv_qty;
            }
            $item->save();
            $item->item->qty = $item->item->qty + $item->rcv_qty;
            $item->item->save();//dd($item);
        } );

        $finrequisition->status = 5;
        $finrequisition->save();
        return redirect()->route('storeapproval.index');;
    }
}
