<?php

namespace App\Http\Controllers\FinishedGoods;
use App\Http\Controllers\Controller;

use App\Models\FinishedGoods\Finrequisition;
use App\Models\FinishedGoods\FinrequisitionItem;
use App\Item;
use App\User;
use App\Formula;
use App\FormulItem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FinrequisitionController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function index()
    {
        $items = Item::all()->where('item_type', '2');

        $finrequisition = finrequisition::with('user')->latest()->whereNotIn('status', [4,5, 2, 1])->get();

        return view( 'finrequisition.index', ['items'=>$items, 'finrequisitions'=>$finrequisition] );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $item = Item::latest()->get()->where('item_type', '2');
       return view( 'finrequisition.create' )->with( 'items', $item  );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input_rr = $request->input('finrequisition');
        $input_rr['user_id'] = Auth::id();
        // dd($input_rr);

        $input_rri = $request->input('finrequisition_items');
        // dd($input_rri);

        $finrequisition = finrequisition::create($input_rr);
        $finrequisition->items()->createMany($input_rri);

        return redirect()->route('finrequisition.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\finrequisition  $finrequisition
     * @return \Illuminate\Http\Response
     */
    public function show(finrequisition $finrequisition)
    {
        $finrequisition->load('items.item','user');//lazy load
        return view('finrequisition.view',['finrequisition'=>$finrequisition]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\finrequisition  $finrequisition
     * @return \Illuminate\Http\Response
     */
    public function edit(finrequisition $finrequisition)
    {
        $items = Item::latest()->get()->where('item_type','2');
        $finrequisition->load('items','user');
        return view('finrequisition.edit',['items'=>$items, 'finrequisition'=>$finrequisition ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\finrequisition  $finrequisition
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, finrequisition $finrequisition )
    {
        $finrequisition->items()->delete();
        $finrequisition->items()->createMany( $request->input( 'finrequisition_items' ) );
        return redirect()->route('finrequisition.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\finrequisition  $finrequisition
     * @return \Illuminate\Http\Response
     */
    public function destroy(finrequisition $finrequisition )
    {
        return abort(404);
        $finrequisition->items()->delete();
        $finrequisition->delete();
        return redirect()->route('finrequisition.index');
    }

    public function approve( Request $request, finrequisition $finrequisition )
    {
        // dd( $request->all() );
        $finrequisition->load( 'items' );
        $finrequisition->items->each( function( $item ) use ( $request ) {
            $finrequisitionItems = [];
            if ( $request->has( 'finrequisition_items' ) ) {
                $finrequisitionItems = $request->input( 'finrequisition_items' );
            }
            if ( isset( $finrequisitionItems[$item->id]['aprv_qty'] ) ) {
                $item->aprv_qty = $finrequisitionItems[$item->id]['aprv_qty'];
            } else {
                $item->aprv_qty = $item->qty;
            }
            $item->save();

            $item->item->qty = $item->item->qty - $item->aprv_qty;
            $item->item->save();

        } );

        $formula=Formula::with('formula_item')->get();


        $finrequisition->status = 1;
        $finrequisition->save();
        // $finrequisition->items->qty = $item->item->qty - $item->qty;
        // $finrequisition->items->save();
        return redirect()->route('finrequisition.index');

    }

public function reject( Request $request, finrequisition $finrequisition )
    {
        $finrequisition->load( 'items' );
        $finrequisition->items->each( function( $item ) {
            $item->aprv_qty = 0;
            $item->save();
        } );
        $finrequisition->status = 3;
        $finrequisition->save();
        return redirect()->route('finrequisition.index');;
    }

 // public function purchase( Request $request, finrequisition $finrequisition )
 //    {
 //        // dd( $request->all() );
 //        $finrequisition->load( 'items' );
 //        $finrequisition->items->each( function( $item ) use ( $request ) {
 //            $finrequisitionItems = [];
 //            if ( $request->has( 'finrequisition_items' ) ) {
 //                $finrequisitionItems = $request->input( 'finrequisition_items' );
 //            }
 //            if ( isset( $finrequisitionItems[$item->id]['item_price'] ) ) {
 //                $item->item_price = $finrequisitionItems[$item->id]['item_price'];
 //            } else {
 //                $item->item_price = $item->item_price;
 //            }
 //            $item->save();
 //            //dd($item);
 //        } );

 //        $finrequisition->status = 4;
 //        $finrequisition->save();
 //        return redirect()->route('finrequisition.index');;
 //    }

}
