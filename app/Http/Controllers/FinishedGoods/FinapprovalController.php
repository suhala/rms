<?php

namespace App\Http\Controllers\FinishedGoods;

use App\Http\Controllers\Controller;
use App\Formula;
use App\FormulaItem;
use App\Models\FinishedGoods\Finapproval;
use App\Models\FinishedGoods\Finrequisition;
use App\Models\RawMaterials\Rawrequisition;
use App\Models\FinishedGoods\FinrequisitionItem;
use App\Models\RawMaterials\RawrequisitionItem;
use App\Item;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FinapprovalController extends Controller
{

     public function index()
    {
        $items = Item::all()->where('item_type', '1');
        $finrequisition = Finrequisition::with('user')->latest()->where('status','1')->get();
        return view( 'finapproval.index', ['items'=>$items, 'finrequisitions'=>$finrequisition] );
    }

    public function show(Finrequisition $finrequisition)
    {
        // $finrequisition->load( 'items.item','user' ); //lazy load
        $finrequisition->load( 'items.item.formula.formula_item.raw_item' ); //lazy load
        // dd( $finrequisition );
        $total = 0;
        $qty_remain = 0;
        $item_qty = 0;
        foreach( $finrequisition->items as $fgRequisitionItem ) {
            // dd($fgRequisitionItem->item );
            if( $fgRequisitionItem->item && $fgRequisitionItem->item->formula ) {
                foreach( $fgRequisitionItem->item->formula->formula_item as $formulaItem ) {
                    // dd($formulaItem->raw_item );
                    if( $formulaItem->raw_item ) {
                        $qty_raw=$fgRequisitionItem->qty;
                        $item_qty=$formulaItem->raw_item->qty;
                        $qty_form=$formulaItem->amount;
                        $total=$qty_raw*$qty_form;
                        $qty_remain=$item_qty-$total;

                    }
                }
            }
        }
      //exit();
      // dd($total);
        return view( 'finapproval.view', [
            'finrequisition' => $finrequisition,
            'total' => $total,
            'qty_remain' => $qty_remain,
            'item_qty' => $item_qty
        ] );
    }



    public function send( Request $request, Finrequisition $finrequisition )
    {
        //dd($finrequisition);
        $formula=Formula::with('formula_item')->get();
        $finrequisition->load( 'items' );
        $item_qty = $request->input('itemrequired');

        foreach( $request->itemrequired as $item_id => $item_required ) {

          $items = Item::find($item_id);
          $qty=$items->qty;
          $item_stock=$qty-$item_required;
           $items->qty = $item_stock;
           $items->save();
          // $i_qty->update();
          //dump( 'Item Stock: ' . $item_stock );

        }

        $finrequisition->items->each( function( $item ) use ( $request ) {
            $finrequisitionItems = [];
            if ( $request->has( 'finrequisition_items' ) ) {
                $finrequisitionItems = $request->input( 'finrequisition_items' );
            }
            $item->save();
        } );

        $finrequisition->status = 4;
        $finrequisition->save();
        return redirect()->route('finapproval.index');

    }

}
