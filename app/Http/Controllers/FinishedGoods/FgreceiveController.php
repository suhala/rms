<?php

namespace App\Http\Controllers\FinishedGoods;
use App\Http\Controllers\Controller;

use App\Models\FinishedGoods\Finapproval;
use App\Models\FinishedGoods\Finrequisition;
use App\Models\FinishedGoods\FinrequisitionItem;
use App\Item;
use App\User;
use App\Models\FinishedGoods\Sale;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FgreceiveController extends Controller
{

     public function index()
    {
        $items = Item::all()->where('item_type', '1');
        $finrequisition = Finrequisition::with('user')->latest()->where('status','5')->get();
        return view( 'fgreceive.index', ['items'=>$items, 'finrequisitions'=>$finrequisition] );
    }

   public function show(Finrequisition $finrequisition)
    {
        $finrequisition->load('items.item','user');//lazy load
        // dd($finrequisition);
        return view('fgreceive.view',['finrequisition'=>$finrequisition]);
    }


 public function purchase( Request $request, Finrequisition $finrequisition )
    {
        // dd( $request->all() );
        $finrequisition->load( 'items' );
        $finrequisition->items->each( function( $item ) use ( $request ) {
            $finRequisitionItems = [];
            if ( $request->has( 'finrequisition_items' ) ) {
                $finRequisitionItems = $request->input( 'finrequisition_items' );
            }
            if ( isset( $finRequisitionItems[$item->id]['item_price'] ) ) {
                $item->item_price = $finRequisitionItems[$item->id]['item_price'];
            } else {
                $item->item_price = $item->item_price;
            }
            $item->save();
            //dd($item);
        } );

        $finrequisition->status = 2;
        $finrequisition->save();
        return redirect()->route('fgreceive.index');;
    }

}
