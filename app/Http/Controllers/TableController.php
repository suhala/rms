<?php

namespace App\Http\Controllers;

use App\Table;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class TableController extends Controller
{

    public function index()
    {
      $room = Room::with('floor')->get();
      // $table = Table::latest()->with('room.floor')->get();
      $table = Table::latest()->get();
      $table->load('room.floor');
     // $table = Table::latest()->with('room')->get();
      return view( 'table.index' )->with(['rooms'=> $room , 'tables'=> $table]);
    }

    public function create()
    {
       $rooms = Room::latest()->with('floor')->get();
       return view( 'table.create')->with( 'rooms', $rooms  );
    }

    public function store(Request $request)
    {

        $input=$this->validate($request,[
            'table_name' => 'required'

        ]);

        $input['room_id'] = $request->room_id;
        $input['status'] = 0;
        // $input->status=0;
        //dd($request->all());
        $table = Table::create( $input );
       // dd($input);

      return redirect()->route('table.index');
    }

    public function show(Table $table)
    {
        //
    }

    public function edit(Table $table)
    {
        $rooms = Room::latest()->with('floor')->get();
        $table->load('room.floor');
        return view('table.edit',['rooms'=> $rooms , 'table'=> $table]);

    }


    public function update(Request $request, table $table)
    {
        $table->update(
        $request->all()
        );

        return redirect()->route('table.index');
    }

    public function destroy(table $table)
    {
        $table->delete();
        return redirect()->route('table.index');
    }
}