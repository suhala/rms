<?php

namespace App\Http\Controllers;

use App\Site;
use App\Item;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('cat_type', 2)->get();
        // dd($categories);
         return view('site.index')->with( ['categories' => $categories]);

    }

    // public function show(Site $site)
    // {
    //    $categories = Category::where('cat_type', 2)->get();
    //    $item = Item::with('category.parent','unit')->latest()->where('item_type', 2)->get();

    //     return view( 'site.view')
    //        ->with( ['type'=>$type ,'item' => $item] );
    // }


}
