<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    public function index( $type )
    {
       // $category = Category::with( 'parent' )->where('cat_type', '1')->latest()->get();
       $categories = Category::with( 'parent' )->where('cat_type', $type)->latest()->get();
       return view( 'category.index' )
            ->with( 'type', $type  )
            ->with( 'categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $type )
    {
        $category = Category::latest()->where('cat_type', $type)->get();
        return view( 'category.create' )
            ->with( 'type', $type  )
            ->with( 'categories', $category  );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type)
    {

        $input=$this->validate($request,[
            'category_name' => 'required',
            'sub_cat' => 'nullable',
            'category_img' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $image=$request->file('category_img');
        $new_name=rand() . '.' . $image->getClientOriginalExtension();
        $img_url='images/'.$new_name;
        $image->move(public_path("images"),$new_name);

        $input['cat_type'] = $type;
        $input['category_img'] = $img_url;

        //dd($input);

        $category = Category::create($input);

      return redirect()->route('category.index', ['type'=>$type])->with( 'msg', "Success")->with('path',$new_name);;
    }

    /**
     * Display the specified resource.category
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $type, Category $category )
    {
     return view('category.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($type, Category $category )
    {
        $categories = Category::latest()->get();
        return view('category.edit',['category'=>$category, 'categories'=>$categories, 'type'=>$type ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $type, Category $category)
    {
       // dd($type, $category->cat_type);
     if ( $type != $category->cat_type ) {

           return redirect()
                ->route( 'category.index', [ 'type' => $category->cat_type ] )
                ->with( 'msg', 'type not matched' );
        }
        $category->update(
        $request->all()
        );

      return redirect()->route('category.index', ['type'=>$type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($type, Category $category)
    {
        $category->delete();
        return redirect()->route('category.index', ['type'=>$type]);
    }
}