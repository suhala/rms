<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SupplierController extends Controller
{
    /**
     * Display a li
     sting of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplier = Supplier::all();
        return view( 'supplier.index' )->with( 'suppliers', $supplier  );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //$supplier = Supplier::latest()->where('cat_type', '1')->get();
       return view( 'supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $input=$this->validate($request,[
            'name' => 'required',
            'address' => 'nullable',
            'contact' => 'required',
            'email' => 'nullable'
        ]);

        $supplier = Supplier::create($input);

      return redirect()->route('supplier.index');
    }

    /**
     * Display the specified resource.category
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        $suppliers = Supplier::latest()->get();
        return view('supplier.edit',['supplier'=>$supplier, 'suppliers'=>$suppliers]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
         $supplier->update($request->all());

         return redirect()->route('supplier.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
         $supplier->delete();
         return redirect()->route('supplier.index');
    }
}