<?php

namespace App\Http\Controllers\RawMaterials;
// namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\RawMaterials\Rawrequisition;
use App\Models\RawMaterials\RawrequisitionItem;
use App\Item;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class RawrequisitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all()->where('item_type', '1');
        // $rawrequisition = Rawrequisition::with('items', 'user')->latest()->get();
        // $rawrequisition = Rawrequisition::with('user')->latest()->where(['status', '1'],['status', '0'],['status', '3'])->get();
        $rawrequisition = Rawrequisition::with('user')->latest()->whereNotIn('status', [4, 2, 1])->get();

        return view( 'rawrequisition.index', ['items'=>$items, 'rawrequisitions'=>$rawrequisition] );
    }


    public function create()
    {
       $item = Item::latest()->get()->where('item_type', '1');
       return view( 'rawrequisition.create' )->with( 'items', $item  );
    }


    public function store(Request $request)
    {

        $input_rr = $request->input('rawrequisition');
        $input_rr['user_id'] = Auth::id();
        // dd($input_rr);

        $input_rri = $request->input('rawrequisition_items');
        // dd($input_rri);

        $rawrequisition = Rawrequisition::create($input_rr);
        $rawrequisition->items()->createMany($input_rri);

        return redirect()->route('rawrequisition.index');
    }


    public function show(Rawrequisition $rawrequisition)
    {
        $rawrequisition->load('items.item','user');//lazy load
        return view('rawrequisition.view',['rawrequisition'=>$rawrequisition]);
    }


    public function edit(Rawrequisition $rawrequisition)
    {
        $items = Item::latest()->get()->where('item_type','1');
        $rawrequisition->load('items','user');
        return view('rawrequisition.edit',['items'=>$items, 'rawrequisition'=>$rawrequisition ]);
    }


    public function update( Request $request, Rawrequisition $rawrequisition )
    {
        $rawrequisition->items()->delete();
        $rawrequisition->items()->createMany( $request->input( 'rawrequisition_items' ) );
        return redirect()->route('rawrequisition.index');
    }


    public function destroy(Rawrequisition $rawrequisition )
    {
        return abort(404);
        $rawrequisition->items()->delete();
        $rawrequisition->delete();
        return redirect()->route('rawrequisition.index');
    }

    public function approve( Request $request, Rawrequisition $rawrequisition )
    {
        //dd( $request->all() );
        $rawrequisition->load( 'items' );
        $rawrequisition->items->each( function( $item ) use ( $request ) {
            $rawRequisitionItems = [];
            if ( $request->has( 'rawrequisition_items' ) ) {
                $rawRequisitionItems = $request->input( 'rawrequisition_items' );
            }
            if ( isset( $rawRequisitionItems[$item->id]['aprv_qty'] ) ) {
                $item->aprv_qty = $rawRequisitionItems[$item->id]['aprv_qty'];
            } else {
                $item->aprv_qty = $item->qty;
            }


            // dd($item);
            $item->save();
        } );
        /*if ( $request->has( 'rawrequisition_items' ) ){
            foreach( $request->rawrequisition_items as $item_id => $quantity ) {
                $rri = RawrequisitionItem::find( $item_id );
                $rri->update( [
                    'aprv_qty' => $quantity['aprv_qty']
                ] );
            }
        }*/
        $rawrequisition->status = 1;
        $rawrequisition->save();
        return redirect()->route('rawrequisition.index');;
    }

public function reject( Request $request, Rawrequisition $rawrequisition )
    {
        $rawrequisition->load( 'items' );
        $rawrequisition->items->each( function( $item ) {
            $item->aprv_qty = 0;
            $item->save();
        } );
        $rawrequisition->status = 3;
        $rawrequisition->save();
        return redirect()->route('rawrequisition.index');;
    }
}
