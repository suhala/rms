<?php

namespace App\Http\Controllers\RawMaterials;

use App\Http\Controllers\Controller;
use App\Models\RawMaterials\Approval;
use App\Models\RawMaterials\Rawrequisition;
use App\Models\RawMaterials\RawrequisitionItem;
use App\Item;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ApprovalController extends Controller
{

     public function index()
    {
        $items = Item::all()->where('item_type', '1');
        $rawrequisition = Rawrequisition::with('user')->latest()->where('status','1')->get();
        return view( 'approval.index', ['items'=>$items, 'rawrequisitions'=>$rawrequisition] );
    }

   public function show(Rawrequisition $rawrequisition)
    {
        $rawrequisition->load('items.item','user');//lazy load
        // dd($rawrequisition);
        return view('approval.view',['rawrequisition'=>$rawrequisition]);
    }


 public function purchase( Request $request, Rawrequisition $rawrequisition )
    {
        // dd( $request->all() );
        // dd(
        //     $request->has( 'rawrequisition_items.15.item_price' ),
        //     $request->input( 'rawrequisition_items.15' )
        // );
        $rawrequisition->load( 'items' );
        $rawrequisition->items->each( function( $item ) use ( $request ) {

            if ( $request->has( "rawrequisition_items.{$item->id}.item_price" ) ) {
                $item->item_price = $request->input( "rawrequisition_items.{$item->id}.item_price" );
            }
            // $item->subtotal = $rawRequisitionItems[$item->id]['subtotal'];
            $item->subtotal = $request->input( "rawrequisition_items.{$item->id}.subtotal" );
            $item->save();
            //dd($item);
        } );
        $rawrequisition->total = $request->total;

        $rawrequisition->status = 4;
        $rawrequisition->save();
        return redirect()->route('approval.index');
    }

}
