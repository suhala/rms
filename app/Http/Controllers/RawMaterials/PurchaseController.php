<?php

namespace App\Http\Controllers\RawMaterials;

use App\Http\Controllers\Controller;
use App\Models\RawMaterials\Rawrequisition;
use App\Models\RawMaterials\Purchase;
use App\Models\RawMaterials\RawrequisitionItem;
use App\Item;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PurchaseController extends Controller
{

    public function index()
    {
        $items = Item::all()->where('item_type', '1');
        $rawrequisition = Rawrequisition::with('user')->latest()->where('status','4')->get();
        return view( 'purchase.index', ['items'=>$items, 'rawrequisitions'=>$rawrequisition] );
    }

    public function show(Rawrequisition $rawrequisition)
    {
        $rawrequisition->load('items.item','user');//lazy load
        // dd($rawrequisition);
        return view('purchase.view',['rawrequisition'=>$rawrequisition]);
    }


    public function receive( Request $request, Rawrequisition $rawrequisition )
    {
        $rawrequisition->load( 'items.item' );
        $rawrequisition->items->each( function( $item ) use ( $request ) {
            $rawRequisitionItems = [];
            if ( $request->has( 'rawrequisition_items' ) ) {
                $rawRequisitionItems = $request->input( 'rawrequisition_items' );
            }
            if ( isset( $rawRequisitionItems[$item->id]['purchase_price'] ) ) {
                $item->item_price = $rawRequisitionItems[$item->id]['purchase_price'];
            } else {
                $item->item_price = $item->purchase_price;
            }
            if ( isset( $rawRequisitionItems[$item->id]['rcv_qty'] ) ) {
                $item->rcv_qty = $rawRequisitionItems[$item->id]['rcv_qty'];
            } else {
                $item->rcv_qty = $item->aprv_qty;
            }
            $item->subtotal_purchase = $rawRequisitionItems[$item->id]['subtotal_purchase'];

            $item->save();
            $item->item->qty = $item->item->qty + $item->rcv_qty;
            $item->item->save();
        } );

        $rawrequisition->status = 2;
        $rawrequisition->total_purchase = $request->input( 'total_purchase');
        $rawrequisition->save();
        return redirect()->route('purchase.index');;
    }

}
