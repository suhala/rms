<?php

namespace App\Http\Controllers\RawMaterials;

use App\Http\Controllers\Controller;
use App\Models\RawMaterials\Approval;
use App\Models\RawMaterials\Rawrequisition;
use App\Models\RawMaterials\RawrequisitionItem;
use App\Item;
use App\User;
use App\Models\RawMaterials\Receive;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ReceiveController extends Controller
{

     public function index()
    {
        $items = Item::all()->where('item_type', '1');
        $rawrequisition = Rawrequisition::with('user')->latest()->where('status','2')->get();
        return view( 'receive.index', ['items'=>$items, 'rawrequisitions'=>$rawrequisition] );
    }

   public function show(Rawrequisition $rawrequisition)
    {
        $rawrequisition->load('items.item','user');//lazy load
        // dd($rawrequisition);
        return view('receive.view',['rawrequisition'=>$rawrequisition]);
    }


 public function purchase( Request $request, Rawrequisition $rawrequisition )
    {
        // dd( $request->all() );
        $rawrequisition->load( 'items' );
        $rawrequisition->items->each( function( $item ) use ( $request ) {
            $rawRequisitionItems = [];
            if ( $request->has( 'rawrequisition_items' ) ) {
                $rawRequisitionItems = $request->input( 'rawrequisition_items' );
            }
            if ( isset( $rawRequisitionItems[$item->id]['item_price'] ) ) {
                $item->item_price = $rawRequisitionItems[$item->id]['item_price'];
            } else {
                $item->item_price = $item->item_price;
            }
            $item->save();
            //dd($item);
        } );

        $rawrequisition->status = 4;
        $rawrequisition->save();
        return redirect()->route('receive.index');;
    }

}
