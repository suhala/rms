<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all()->where('item_type', '2');
        $orders = Order::with('items')->latest()->get();
        // dd( $order );  

        return view( 'order.index', ['items'=> $items , 'orders'=> $orders] );
    }
 // 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::latest()->where( 'item_type', "2" )->get();
        return view( 'order.create')
        ->with( 'items', $items );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $input_o = [
        //     'user_id' => Auth::id()
        // ];

        $order = Order::create();

        $order->items()->createMany( $request->input( 'items' ) );

        // dd($request->all());
       // $input_rr = $request->input('order');
       //  //$input_rr['user_id'] = Auth::id();
       //  // dd($input_rr);

       //  $input_rri = $request->input('order_items');
       //  // dd($input_rri);

       //  $orders = Order::create($input_rr);
       //  $orders->items()->createMany($input_rri);

         return redirect()->route('order.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
          $orders = Order::all();
          return view('order.edit',['order'=>$order, 'orders'=>$orders]);
    }
        
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
         $order->update(
         $request->all()
         );

         return redirect()->route('order.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
          $order->delete();
          return redirect()->route('order.index');
    }
}
