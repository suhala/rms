<?php

namespace App\Http\Controllers;

use App\Item;
use App\Category;
use App\Unit;
use App\Table;
use App\Cusorder;
use App\CusorderDetails;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CusOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $items = Item::all()->where('item_type', '2');
       $cusorders = Cusorder::with('table')->latest()->get();
       return view( 'cusorder.index')->with( ['cusorders' => $cusorders, 'items' => $items ] );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( )
    {
       $items = Item::latest()->get()->where('item_type', '2');
       $tables = Table::latest()->get();
        return view( 'cusorder.create' )->with( ['tables' => $tables, 'items' => $items ]   );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_rr = $request->input('cusorder');

   // dd($request->all());

        $input_rri = $request->input('cusorder_details');
        // dd($input_rri);

        $cusorder = Cusorder::create($input_rr);
        $cusorder->items()->createMany($input_rri);

        return redirect()->route('cusorder.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Cusorder $cusorder)
    {
        $items = Item::latest()->get()->where('item_type','2');
        $tables = Table::latest()->get();
        $cusorder->load('items','table');
        return view('cusorder.edit',['items'=>$items, 'cusorder'=>$cusorder, 'tables' => $tables]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item, Cusorder $cusorder)
    {
        // dd( $request->all() );
        $cusorder->update( $request->cusorder );
        $cusorder->items()->delete();
        $cusorder->items()->createMany( $request->input( 'cusorder_details' ) );
        return redirect()->route('cusorder.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($type, Item $item)
    {
        $item->delete();
        return redirect()->route('item.index',  ['type'=>$type ]);
    }

    public function getPrice(Request $item_id)
    {
        $data = Item::find($item_id->data);
        echo ($data->price);
    }
}
