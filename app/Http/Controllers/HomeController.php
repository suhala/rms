<?php

namespace App\Http\Controllers;

use App\Table;
use App\Room;
use App\Floor;
use App\Item;
use App\User;
use App\Models\RawMaterials\Rawrequisition;
use App\Models\FinishedGoods\Finrequisition;


use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


         $item_count = Item::where('item_type', '1')->count();
         $req_count = Rawrequisition::whereNotIn('status', [4, 3, 2, 1])->count();
         $order_count = Finrequisition::where('status', '4')->count();
         $user_count = User::count();
         $floors = Floor::with( 'rooms.tables' )->get();
         // dd($floors);
         return view('home')->with(['floors'=>$floors,'item_count'=>$item_count,'req_count'=>$req_count,'order_count'=>$order_count,'user_count'=>$user_count ]);

    }

    public function test()
    {
        $data01 = 'This is data 01';
        $data02 = 'This is data 02';
        // return view( 'test' )->with( 'data01', $data01 )->with( 'data02', $data02  );
        /*return view( 'test' )->with( [
            'data01' => $data01,
            'data02' => $data02
        ] );*/
        return view( 'test', compact( 'data01' ) );
    }


}
