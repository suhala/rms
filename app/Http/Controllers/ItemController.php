<?php

namespace App\Http\Controllers;

use App\Item;
use App\Category;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $type)
    {
       $categories = Category::where('cat_type', $type)->get();
       $item = Item::with('category.parent','unit')->latest()->where('item_type', $type)->get();
       $units = Unit::all();

        return view( 'item.index')
           ->with( ['type'=>$type ] )
           ->with( ['categories' => $categories, 'item' => $item, 'units' => $units ] );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $type )
    {
        $units = Unit::with('items_unit')->get();

        $categories = Category::latest()->where( 'cat_type', $type )->get();
        return view( 'item.create' )
            ->with( 'type', $type )
            ->with( ['categories' => $categories, 'units' => $units ]   );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type)
    {
        $input = $this->validate($request,[
            'item_name' => 'required',
            'category_id' => 'required',
            'unit_id' => 'required',
            'item_img' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $image=$request->file('item_img');
        $new_name=rand() . '.' . $image->getClientOriginalExtension();
        $img_url='images/'.$new_name;
        $image->move(public_path("images"),$new_name);


        $input['item_type'] = $type;
        $input['description'] = $request->description;
        $input['price'] = $request->price;
        $input['item_img'] = $img_url;



        $item = Item::create( $input );
        //dd($item);
        return redirect()->route('item.index', ['type'=>$type ] );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($type, Item $item)
    {
        $categories = Category::all();
        $units = Unit::with('items_unit')->get();
        $item->load('category');
        return view('item.edit',['item'=>$item, 'units' => $units, 'categories'=>$categories, 'type'=>$type ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $type, Item $item)
    {
        $category = Category::all();
        if ( $type != $item->item_type ) {
            return redirect()
                ->route( 'item.index', [ 'type' => $item->item_type ] )
                ->with( 'msg', 'type not matched' );
        }

        $item->update(
        $request->all()
        );

        return redirect()->route('item.index',  ['type'=>$type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($type, Item $item)
    {
        $item->delete();
        return redirect()->route('item.index',  ['type'=>$type ]);
    }
}
