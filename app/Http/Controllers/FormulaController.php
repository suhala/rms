<?php

namespace App\Http\Controllers;

use App\Formula;
use App\FormulaItem;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FormulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //$formulas = Formula::with('formula_item.raw_items')->latest()->get();
       // dd(Formula::find(1)->load('fin_item'));
       $items = Item::with('formula.fin_item')->where('item_type', '2')->get();
       $formulas = Formula::with('formula_item','fin_item')->get();//dd($items);
       // return view( 'formula.index' )->with( ['items'=> $items, 'formulas'=> $formulas] );
       return view( 'formula.index' )->with( ['items'=> $items,'formulas'=> $formulas] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $ingredients = Item::latest()->get()->where('item_type', '1');
       $items = Item::latest()->get()->where('item_type', '2');
       return view( 'formula.create' )->with( ['ingredients'=> $ingredients , 'items'=> $items] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    // $input=$this->validate($request,[
    //     'item_id' => 'required'
    // ]);
   // dd($request->all());
    $input = $request->input('formulas');

    $input_formula_items = $request->input('formulas_item');

    $formula = Formula::create($input);
    $formula->formula_item()->createMany($input_formula_items);
    // dd($formula);
    return redirect()->route('formula.index');
    }

    /**
     * Display the specified resource.category
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Formula $formula)
    {
        $formula->load('formula_item.raw_items','fin_item');//lazy load
        return view('formula.view',['formula'=>$formula]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Formula $formula)
    {
        $ingredients = Item::latest()->get()->where('item_type', '1');
        $items = Item::latest()->get()->where('item_type', '2');
        $formula->load('formula_item','fin_item');
        return view('formula.edit',[ 'ingredients'=>$ingredients,'items'=>$items, 'formula'=>$formula ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Formula $formula)
    {
        //dd($request);
        //$formula->formula_item()->delete();
        //$formula->formula_item()->createMany( $request->input( 'formulas_item' ) );
        //return redirect()->route('formula.index');
        $formula->formula_item()->delete();
        $formula->formula_item()->createMany( $request->input( 'formulas_item' ) );
        return redirect()->route('formula.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Formula $formula)
    {
        $formula->formula_item()->delete();
        $formula->delete();
        return redirect()->route('formula.index');
    }
}