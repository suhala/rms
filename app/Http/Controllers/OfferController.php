<?php

namespace App\Http\Controllers;

use App\Offer;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $items = Item::all()->where('item_type');
       $offer = Offer::with('item')->latest()->get();
       return view( 'offer.index' )
            ->with( 'item', $items )
            ->with( 'offers', $offer);
    }
       //$categories = Category::all()->where('cat_type', $type);
       //$item = Item::with('category')->latest()->where('item_type', $type)->get();
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $items = Item::latest()->where( 'item_type', "2" )->get();
       return view( 'offer.create')
          ->with( 'items', $items );

    }
       // $categories = Category::latest()->where( 'cat_type', $type )->get();
       //  return view( 'item.create' )
       //      ->with( 'type', $type )
       //      ->with( 'categories', $categories  );
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input=$this->validate($request,[
            'item_name' => 'required',
            'price' => 'nullable',
            'discount_rate' => 'required',
            'discount_price' => 'nullable',
            'date' => 'required',
            'duration' => 'nullable'
        ]);

        $offer = Offer::create($input);

      return redirect()->route('offer.index');
    }

    /**
     * Display the specified resource.category
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(offer $offer)
    {
        $offers = Offer::latest()->get();
        return view('offer.edit',['offer'=>$offer, 'offers'=>$offers]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        $offer->update(
        $request->all()
        );

        return redirect()->route('offer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        $offer->delete();
        return redirect()->route('offer.index');
    }
}