<?php

namespace App\Http\Controllers;

use App\Floor;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FloorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $floor = Floor::all();
       return view( 'floor.index' )->with( 'floors', $floor );
    }
//
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //$offer = Offer::latest()->where('cat_type', '1')->get();
       return view( 'floor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input=$this->validate($request,[
            'floor_name' => 'required'
        ]);

        $Floor = Floor::create($input);


      return redirect()->route('floor.index');
    }

    /**
     * Display the specified resource.category
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Floor $floor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Floor $floor)
    {
        $floors = Floor::latest()->get();
        return view('floor.edit',['floor'=>$floor, 'floors'=>$floors]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Floor $floor)
    {
        $floor->update(
        $request->all()
        );

        return redirect()->route('floor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Floor $floor)
    {
        $floor->delete();
        return redirect()->route('floor.index');
    }
}