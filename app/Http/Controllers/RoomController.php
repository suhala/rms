<?php

namespace App\Http\Controllers;

use App\Room;
use App\Floor;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class RoomController extends Controller
{

    public function index()
    {
       $floors = Floor::all();
       // dd($floors);
       $room = Room::with('floor')->latest()->get();
       //dd($room);

       return view( 'room.index')->with(['rooms'=> $room , 'floors'=> $floors] );
    }

    public function create()
    {
        $floors = Floor::latest()->get();
        return view( 'room.create' )
            ->with( 'floors', $floors  );
    }

    public function store(Request $request)
    {
        $input = $this->validate($request,[
            'room_name' => 'required',
            'floor_id' => 'required',
        ]);

        $room = room::create( $input );
        return redirect()->route('room.index');
    }

    public function show(Room $room)
    {
        //
    }

    public function edit(Room $room)
    {
        $floors = Floor::all();
        $room->load('floor');
        return view('room.edit',['room'=>$room, 'floors'=>$floors]);
    }

    public function update(Request $request, Room $room)
    {
        $floor = Floor::all();
        $room->update($request->all());

        return redirect()->route('room.index');
    }

    public function destroy(Room $room)
    {
        $room->delete();
        return redirect()->route('room.index');
    }
}
