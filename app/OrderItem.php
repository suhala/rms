<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';
    protected $fillable = ['item_id', 'qty'];
    
    public function item()
     {
     	return $this->belongsTo( 'App\Item');
     }
}
