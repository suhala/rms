<?php

namespace App\Models\RawMaterials;

use Illuminate\Database\Eloquent\Model;

class RawrequisitionItem extends Model
{

    protected $table = 'rawrequisitions_item';
    protected $fillable = [ 'item_id', 'qty', 'aprv_qty', 'subtotal', 'subtotal_purchase' , 'total', 'total_purchase' ];

     public function item()
    {
    	return $this->belongsTo('App\Item');
    }
}
