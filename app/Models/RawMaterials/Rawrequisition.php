<?php

namespace App\Models\RawMaterials;

use Illuminate\Database\Eloquent\Model;

class Rawrequisition extends Model
{
    protected $table = 'rawrequisitions';
    protected $fillable = [ 'user_id' ];

    public function items()
    {
    	return $this->hasMany('App\Models\RawMaterials\RawrequisitionItem');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function getFormattedStatusAttribute()
    {
        $status = '';
        if($this->status==1){
            $status='Approved';
        } elseif($this->status==2){
            $status='Received';
        } else {
            $status='Pending';
        }
        return $status;
    }

    public function getIsApprovedAttribute()
    {
        return $this->status==1;
    }

    public function getMetaAttribute()
    {
        $data = (object) [
            'formattedStatus' => 'Pending',
            'isApproved' => false,
        ];
        if($this->status==1){
            $data->formattedStatus='Approved';
        } elseif($this->status==2){
            $data->formattedStatus='Received';
        } elseif($this->status==3){
            $data->formattedStatus='Rejected';
        } elseif($this->status==4){
            $data->formattedStatus='Purchase Ordered';
        }

        if($this->status==1){
            $data->isApproved=true;
        }
        return $data;
    }

}
