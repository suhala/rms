<?php

namespace App\Models\FinishedGoods;

use Illuminate\Database\Eloquent\Model;

class FinrequisitionItem extends Model
{

    protected $table = 'finrequisitions_item';
    protected $fillable = [ 'item_id', 'qty', 'aprv_qty' ];

     public function item()
    {
    	return $this->belongsTo('App\Item');
    }
}
