<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formula extends Model
{

    protected $table = 'formulas';

    protected $fillable = [
         'item_id'
    ];

    // protected $hidden = [];

    public function formula_item()
    {
        return $this->hasmany( 'App\FormulaItem' );
    }

    public function fin_item()
    {
    	return $this->belongsTo( 'App\Item', 'item_id', 'id' );
    }

}
