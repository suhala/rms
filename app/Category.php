<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [

         'category_name','category_img', 'sub_cat','cat_type'
    ];


    protected $hidden = [

    ];

    public function parent()
    {
    	return $this->belongsTo( 'App\Category', 'sub_cat' );
    }

    public function menu_items()
    {
        return $this->hasMany( 'App\Item');
    }
}
