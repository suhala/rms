<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'units';
    protected $fillable = [
         'unit_name'
    ];

    protected $hidden = [

    ];

 
    public function items_unit()
    {
        return $this->hasMany('App\Item');
    }
}
