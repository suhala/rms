<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'AdminLTE 2',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>Admin</b>',

    'logo_mini' => '<b><i class="fa fa-user"></i></b>',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'blue',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

'menu' => [
    'home' => [
            'text'    => 'HOME',
            'icon'    => 'home',
            'url'     => '/home'
            ],
        'INVENTORY',
         // [
            // 'text'    => 'INVENTORY',
            // 'icon'    => 'houzz',
            // 'submenu' => [

                [
                    'text'    => 'FINISHED GOODS',
                    'icon'    => 'cube',
                    'icon_color' => 'red',
                    'submenu' => [
                        [
                            'text' => 'Requisition List',
                            'icon' => '',
                            'url'  => '/finrequisition',
                        ],
                        [
                            'text' => 'Store Supply(RM)',
                            'icon' => '',
                            'url'  => '/finapproval',
                        ],
                        [
                            'text' => 'Production(FG)',
                            'icon' => '',
                            'url'  => '/storeapproval',
                        ],
                        [
                            'text' => 'Delivered Product',
                            'icon' => '',
                            'url'  => '/fgreceive',
                        ],
                    ],
                ],
                [
                    'text'    => 'RAW MATERIALS',
                    'icon'    => 'cog',
                    'icon_color' => 'yellow',
                    'submenu' => [
                        [
                            'text' => 'Requisition List',
                            'icon' => '',
                            'url'  => '/rawrequisition',
                        ],
                        [
                            'text' => 'Approved Requisitions',
                            'icon' => '',
                            'url'  => '/approval',
                        ],
                        [
                            'text' => 'Purchase Orders',
                            'icon' => '',
                            'url'  => '/purchase',
                        ],
                        [
                            'text' => 'Material Receive',
                            'icon' => '',
                            'url'  => '/receive',
                        ],
                    ],
                ],

           // ],
        // ],

    'SETTINGS',

                [
                    'text'    => 'FINISHED GOODS',
                    'icon'    => 'cube',
                    'icon_color' => 'red',
                    'submenu' => [
                        [
                            'text' => 'Category',
                            'icon' => '',
                            'url'  => '/category/2',
                        ],
                        [
                            'text' => 'Item',
                            'icon' => '',
                            'url'  => '/item/2',
                        ],
                        [
                            'text' => 'Offer',
                            'icon' => '',
                            'url'  => '/offer',
                        ],
                    ],
                ],
                [
                    'text'    => 'RAW MATERIALS',
                    'icon'    => 'cog',
                    'icon_color' => 'yellow',
                    'submenu' => [
                        [
                            'text' => 'Category',
                            'icon' => '',
                            'url'  => '/category/1',
                        ],
                        [
                            'text' => 'Item',
                            'icon' => '',
                            'url'  => '/item/1',
                        ],
                    ],
                ],
                [
                    'text'    => 'MASTER',
                    'icon'    => 'gavel ',
                    'icon_color' => 'green',
                    'submenu' => [
                        [
                            'text'    => 'FORMULA',
                            'icon'    => '',//calculator
                            'url'  => '/formula',
                        ],
                        [
                            'text' => 'Unit',
                            'icon' => '',//tachometer
                            'url'  => '/unit',
                        ],
                        [
                            'text'    => 'SUPPLIER',
                            'icon'    => '',//user,
                            'url'  => '/supplier',
                        ],

                        [
                            'text' => 'TABLE',
                            'icon' => '',
                            'url'  => '/table',
                        ],
                        [
                            'text' => 'ROOM',
                            'icon' => '',
                            'url'  => '/room',
                        ],
                        [
                            'text' => 'FLOOR',
                            'icon' => '',
                            'url'  => '/floor',
                        ],
                            ],
                ],
               'RESERVATION',

                 [
                    'text' => 'CUSTOMER ORDER',
                    'icon' => 'first-order',
                    'icon_color' => 'purple',
                    'url'  => '/cusorder',
                ],

        'LABELS',
        [
            'text'       => 'Site link',
            'icon_color' => 'red',
            'url'  => '/website',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
    ],
];
